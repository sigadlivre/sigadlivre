<?php
include_once "headers.php";
include_once "utilitarios.php";
include_once "../modelo/Processo_Documento.php";

require_once "valida-token.php";
$operacao = $_REQUEST["operacao"];

$processo_documento = new Processo_Documento();
//usuario que fez a requisição
$usuarioRequest = $processo_documento->getUsuario();

if ($operacao == "criar") {

    $processo_documento->lerFormulario();

    $processo_documento->iniciarTrans(); // iniciar transação aqui pq a tabela de processo pode ser editada e nao dar certo de inserie
    if ($processo_documento->verificaMeio()) {
        $id = $processo_documento->INSERT();
        if ($id) {
            $processo_documento->comitarTrans();
            imprimirSaida("id", $id, true);
        }
    }
    //se der certo ele entra no if imprime a saida e da o die  e nao "bate" nessa parte do erro
    $processo_documento->voltarTrans();
    imprimirSaida("erro", "Erro ao inserir o documento no processo.", false);

}