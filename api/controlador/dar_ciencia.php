<?php
require_once "headers.php";
require_once "utilitarios.php";
require_once "../modelo/Usuario.php";
require_once "../modelo/Proc_Doc_Ciencia.php";
require_once "../modelo/Processo.php";
require_once "../modelo/Documento.php";

require_once "valida-token.php";
$operacao = $_REQUEST["operacao"];

if ($operacao == "dar_ciencia") {
    $objeto = new Processo();

    //usuario que fez a requisição
    $usuarioRequest = $objeto->getUsuario();

    $obj_ciencia = new Proc_Doc_Ciencia();
    $usuario = new Usuario();

    //pegando campos da requisição
    $campos = $objeto->getDataRequest();

    $parametros = array(
        "tipo",
        "usuario",
        "data",
    );

    foreach ($parametros as $parametro) {
        if (!isset($campos->$parametro)) {
            imprimirSaida("erro", "{$parametro} indefinido.", false);
        }
    }

    if ($campos->tipo == "P") {
        $objeto = new Processo();

        if (isset($campos->id_processo) && ($objeto->verificaSeExiste($campos->id_processo))) {
            $obj_ciencia->id_processo = $campos->id_processo;
        } else {
            imprimirSaida("erro", "Processo Inválido.", false);
        }

    } else if ($campos->tipo == "D") {
        $objeto = new Documento();

        if (isset($campos->id_documento) && ($objeto->verificaSeExiste($campos->id_documento))) {
            $obj_ciencia->id_documento = $campos->id_documento;
        } else {
            imprimirSaida("erro", "Documento Inválido.", false);
        }

    } else {
        imprimirSaida("erro", "Tipo inválido.", false);
    }

    if (!(Processo::validarData($campos->data))) {
        imprimirSaida("erro", "Formato da data inválido.", false);
    }

    if (!($usuario->verificaSeExiste($campos->usuario))) {
        imprimirSaida("erro", "Usuário Inválido.", false);
    }

    $obj_ciencia->usuario = $campos->usuario;
    $obj_ciencia->data = $campos->data;

    $id = $obj_ciencia->INSERT();

    if ($id) {
        imprimirSaida("id", $id, true);
    } else {
        imprimirSaida("erro", "Erro ao dar ciência.", false);
    }

}
