<?php
include_once "headers.php";
include_once "utilitarios.php";
require_once "valida-token.php";
$parametros = array("operacao", "controlador");

foreach ($parametros as $parametro) {
    if (!isset($_REQUEST[$parametro])) {
        imprimirSaida("erro", "{$parametro} indefinido.", false);
    }
}

//usuario que está fazendo a requisição

$controlador = $_REQUEST["controlador"];
$operacao = $_REQUEST["operacao"];

//operações que não podem ser realizadas nesse controlador pois possuem restrições
$controladores = array(
    "Assunto" => array(),
    "Atribuicao_Processo" => array(),
    "Cidade" => array(),
    "Classificacao" => array(),
    "Documento" => array(),
    "Documento_Arquivo" => array(),
    "Documento_Assinatura" => array(),
    "Documento_Fisico" => array(),
    "Documento_Meio" => array(),
    "Documento_Status" => array(),
    "Documento_Template" => array(),
    "Documento_Tipo" => array(),
    "Endereco" => array(),
    "Estado" => array(),
    "Nivel_Acesso" => array(),
    "Nivel_Acesso_Legislacao" => array(),
    "Nup" => array(),
    "Papel" => array(),
    "Pessoa" => array(),
    "Proc_Doc_Ciencia" => array(),
    "Processo" => array("criar"),
    "Processo_Documento" => array("criar", "excluir"),
    "Setor" => array(),
    "Template" => array(),
    "Usuario" => array(),
    "Usuario_Setor" => array(),
    "Processos_Relacionados" => array(),
);

if (array_key_exists($controlador, $controladores)) {

    require_once "../modelo/{$controlador}.php";

    $operacoes_nao_permitidas = $controladores[$controlador];
    $operacoes = array(
        "criar",
        "listar",
        "alterar",
        "excluir",
        "buscar",
    );

    if (in_array($operacao, $operacoes) == false) {
        imprimirSaida("erro", "Operação não encontrada.", false);
    }

    if (in_array($operacao, $operacoes_nao_permitidas)) {
        imprimirSaida("erro", "Operação não permitida nesse controlador.", false);
    }

    $objeto = new $controlador();
    $usuario = $objeto->getUsuario();

    if ($operacao == "criar") {

        $objeto->lerFormulario();
        $id = $objeto->INSERT();

        imprimirSaida("id", $id, true);

    } else if ($operacao == "listar") {

        $limit = array_key_exists('limit', $_REQUEST) ? $_REQUEST['limit'] : null;
        $array = $objeto->SELECT_ALL($usuario->login, $limit);
        imprimirSaida($nomes_para_listagem[$controlador], $array, true);

    } else if ($operacao == "buscar") {

        //verifica se o campo foi mandado, se sim retorna-o, se não gera um erro
        $id = verificaCampo("id");

        if ($objeto->SELECT($id)) {
            imprimirSaida(strtolower($controlador), $objeto, true);
        }
        //se der erro é tratado na classe ADO no método select

    } else if ($operacao == "alterar") {

        verificaCampo("id");
        $id = $_REQUEST["id"];
        $msg = $objeto->UPDATE($id);
        imprimirSaida("mensagem", $msg, true);

    } else if ($operacao == "excluir") {
        verificaCampo("id");

    } else {
        imprimirSaida("erro", "Operação não encontrada", false);
    }

} else {

    imprimirSaida("erro", "Controlador não encontrado", false);
}
