<?php

function __autoload($className)
{
    require_once '../modelo/' . $className . '.php';
}

function verificaCampo($campo)
{
    // request só funciona via get
    if (array_key_exists($campo, $_REQUEST) == true) {
        return $_REQUEST[$campo];

    } else {
        imprimirSaida("erro", "{$campo} indefinido.", false);
        die();
    }
}

$nomes_para_listagem = array(
    "Classificacao" => "classificacoes",
    "Documento_Meio" => "meios",
    "Endereco" => "enderecos",
    "Papel" => "papeis",
    "Template" => "templates",
    "Documento" => "documentos",
    "Estado" => "estados",
    "Pessoa" => "pessoas",
    "Usuario" => "usuarios",
    "Documento_Arquivo" => "",
    "Documento_Status" => "documento_status",
    "Nivel_Acesso_Legislacao" => "niveis_acesso_legislacao",
    "Processo_Documento" => "processo_documentos",
    "Usuario_Setor" => "usuarios_setor",
    "Assunto" => "assuntos",
    "Documento_Assinatura" => "",
    "Documento_Template" => "documento_templates",
    "Nivel_Acesso" => "niveis_acesso",
    "Processo" => "processos",
    "Cidade" => "cidades",
    "Documento_Fisico" => "",
    "Documento_Tipo" => "tipos",
    "Setor" => "setores",
    "Tipo" => "tipos",

);

function imprimirSaida($nome, $objeto, $success)
{
    echo json_encode(array($nome => $objeto, "success" => $success));
    die();
}
