<?php
require_once "headers.php";
require_once "utilitarios.php";

require_once "../modelo/Processo.php";
require_once "valida-token.php";
$operacao = $_REQUEST["operacao"];

$processo = new Processo();
//usuario que fez a requisição
$usuarioRequest = $processo->getUsuario();

if ($operacao == "criar") {

    $processo->iniciarTrans();
    $processo->lerFormulario();

    if (!isset($processo->nup)) {
        $id_processo = $processo->INSERT();

        $nup = new Nup();
        $ano = explode("/", $processo->data_recebimento)[0];

        $nup_gerado = $nup->gerarNup($id_processo, $ano, "PROCESSO");

        $processo->id = $id_processo;
        $processo->nup = $nup_gerado;

        $result = $processo->updateNUP();

        if ($result == false) {
            $processo->voltarTrans();
            imprimirSaida("erro", "Falhou na inserção. Tabela processo", false);
        }

        $processo->comitarTrans();
    } else {

        $nup = new Nup();
        $verificaNup = $nup->verificaNup($processo->nup);
        if ($verificaNup) {
            $id_processo = $processo->INSERT();
        } else {
            imprimirSaida("erro", "Nup inválido", false);
        }
    }

    imprimirSaida("id", $id_processo, true);
}

if ($operacao == "atribuir_usuario_processo") {

    $atr_proc = new Atribuicao_Processo();

    $atr_proc->lerFormulario();

    $usuario = new Usuario();
    $setor = new Setor();

    if (!($processo->verificaSeExiste($atr_proc->id_processo))) {
        imprimirSaida("erro", "Processo Inválido.", false);
    }

    if (!($usuario->verificaSeExiste($atr_proc->usuario_atribuido))) {
        imprimirSaida("erro", "Usuário Inválido.", false);
    }
    if (!($setor->verificaSeExiste($atr_proc->id_setor))) {
        imprimirSaida("erro", "Setor Inválido.", false);
    }

    $id = $atr_proc->INSERT();

    if ($id) {
        imprimirSaida("id", $id, true);
    } else {
        imprimirSaida("erro", "Erro ao atribuir usuário a um processo.", false);
    }

}

if ($operacao == "excluir") {

    $id = $_REQUEST["id"];
    $msg = $processo->DELETE($id);
    imprimirSaida("mensagem", $msg, true);

}

if ($operacao == "tramitar") {

    $tramitacao = new Tramitacao_Processo();
    $tramitacao->lerFormulario();
    $id = $tramitacao->INSERT();
    imprimirSaida("id", $id, true);

}

if ($operacao == "meus_processos") {

    $limit = array_key_exists('limit', $_REQUEST) ? $_REQUEST['limit'] : null;

    $array = $processo->SELECT_MEUS_PROCESSOS($usuarioRequest->login, $limit);
    imprimirSaida("processos", $array, true);

}
if ($operacao == "historico_tramitacao") {

    $id_processo = verificaCampo("id");

    $tramitacao = new Historico_Tramitacao();
    $array = $tramitacao->SELECT_HISTORICO($id_processo, "processo");
    imprimirSaida("historico_tramitacao", $array, true);
}

if ($operacao == "relacionar_processo") {

    $proc_rel = new Processos_Relacionados();
    $proc_rel->lerFormulario();
    $id = $proc_rel->INSERT();
    imprimirSaida("id", $id, true);

}

if ($operacao == "processos_relacionados") {

    $id_processo = verificaCampo("id");

    $processo_relacionado = new Processos_Relacionados();
    $proc_relacionados = $processo_relacionado->SELECT_IN("id_processo", $id_processo);

    $array = $processo->SELECT_IN("id", $proc_relacionados);
    imprimirSaida("processos_relacionados", $array, true);
}