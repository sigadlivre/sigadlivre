<?php
include_once("headers.php");
include_once("../modelo/ADO.php");
include_once("../modelo/Documento.php");
include_once("utilitarios.php");

require_once("valida-token.php");
$operacao = $_REQUEST["operacao"];


$documento = new Documento();

//usuario que fez a requisição 
$usuarioRequest = $documento->getUsuario();

if($operacao == "assinar"){

    $documento      = new Documento();
    $usuario     = new Usuario();
    $doc_assinatura = new Documento_Assinatura();
    
    //pegando campos da requisição
    $doc_assinatura->lerFormulario();
    
    $usuario = new Usuario();
	$setor = new Setor();

	if (!($usuario->verificaSeExiste($doc_assinatura->usuario))) {
        imprimirSaida("erro", "Usuário Inválido.", false);
	}
	if (!($setor->verificaSeExiste($doc_assinatura->id_setor))) {
        imprimirSaida("erro", "Setor Inválido.", false);
    }
    
    if (!($documento->verificaSeExiste($doc_assinatura->id_documento))) {
        imprimirSaida("erro", "Documento Inválido.", false);
    }
    
    
    $id = $doc_assinatura->INSERT();
    
    if ($id) {
        imprimirSaida("id", $id, true);
    } else {
        imprimirSaida("erro", "Erro ao assinar o documento.", false);
    }
}
if($operacao == "autenticar"){

    $documento      = new Documento();
    $usuario     = new Usuario();
    $doc_autenticacao = new Documento_Autenticacao();
    
    //pegando campos da requisição
    $doc_autenticacao->lerFormulario();
    
    $usuario = new Usuario();
	$setor = new Setor();

	if (!($usuario->verificaSeExiste($doc_autenticacao->usuario))) {
        imprimirSaida("erro", "Usuário Inválido.", false);
	}
	if (!($setor->verificaSeExiste($doc_autenticacao->id_setor))) {
        imprimirSaida("erro", "Setor Inválido.", false);
    }
    
    if (!($documento->verificaSeExiste($doc_autenticacao->id_documento))) {
        imprimirSaida("erro", "Documento Inválido.", false);
    }
    
    
    $id = $doc_autenticacao->INSERT();
    
    if ($id) {
        imprimirSaida("id", $id, true);
    } else {
        imprimirSaida("erro", "Erro ao assinar o documento.", false);
    }
}


if($operacao == "pode_editar"){
    $data = $documento->getDataRequest();
    $id_documento = isset($data->id_documento) ? $data->id_documento : imprimirSaida("erro", "id_documento indefinido.", false);
    $pode_editar = $documento->pode_editar($id_documento);
    imprimirSaida("pode_editar", $pode_editar, true);
}


 if($operacao == "documentos_por_processo"){

    //verifica se o campo foi mandado, se sim retorna-o, se não gera um erro
    $id_processo =  verificaCampo("id");

   $array = $documento->SELECT_DOCUMENTOS_POR_PROCESSO($id_processo);
   imprimirSaida("documentos_por_processo", $array, true);

}if($operacao == "documentos_sem_processo"){

   $array = $documento->SELECT_DOCUMENTOS_SEM_PROCESSO();
   imprimirSaida("documentos_sem_processo", $array, true);
}
if($operacao == "buscar"){
    $documento = new Documento();
    $array = array();
    $assuntos = new Assunto();
    $tipos = new Documento_Tipo();
    $niveis_acesso_legislacao = new Nivel_Acesso_Legislacao();
    $meios = new Documento_Meio();
    $setores = new Setor();
    $tramitacao = new Historico_Tramitacao();
	

 
    $id =  verificaCampo("id");
    $documento->SELECT($id);
    $array["documento"] = $documento;
    $array["historico_tramitacao"] = $tramitacao->SELECT_HISTORICO($id, "documento");

    if(isset($documento->id_documento_template)){
        $documento_template = new Documento_Template();
         $documento_template->SELECT($documento->id_documento_template);
         $array["documento_template"] = $documento_template;
     }  
    
    $array["assuntos"] = $assuntos->SELECT_ALL($usuarioRequest->login);
    $array["tipos"] = $tipos->SELECT_ALL($usuarioRequest->login);
    $array["niveis_acesso_legislacao"] = $niveis_acesso_legislacao->SELECT_ALL($usuarioRequest->login);
    $array["meios"] = $meios->SELECT_ALL($usuarioRequest->login);
    $array["setores"] = $setores->SELECT_ALL($usuarioRequest->login);

   
   
    imprimirSaida("dados_documento", $array, true);
}

if($operacao == "tramitar"){

	$tramitacao = new Tramitacao_Documento();
	$tramitacao->lerFormulario();
	$id = $tramitacao->INSERT();
	imprimirSaida("id" ,$id, true);
}
if($operacao == "historico_tramitacao"){
	$id_documento= verificaCampo("id");

	$tramitacao = new Historico_Tramitacao();
	$array = $tramitacao->SELECT_HISTORICO($id_documento, "documento");
	imprimirSaida("historico_tramitacao", $array, true);
}
?>