<?php

include_once "ADO.php";

class Endereco extends ADO
{
    public $id;
    public $nome;
    public $logradouro;
    public $numero;
    public $complemento;
    public $cep;
    public $id_cidade;
    public $id_estado;
}
