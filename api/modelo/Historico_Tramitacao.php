<?php

include_once("ADO.php");

class Historico_Tramitacao extends ADO {
    
    public $origem;
    public $destino;
    public $remetente;
    public $interessado;
    public $data_envio;

    function setarDados($result){
        $campos = get_object_vars($this);
        foreach ($campos as $campo => $valor) {
                
            if (array_key_exists($campo, $result->fields)) {
                if ($campo == "data_envio") {  
                        $result->fields[$campo] =  $this->formatarData($result->fields[$campo], 'd/m/Y H:i:s');
                }
                    $this->$campo = $result->fields[$campo];           
            }else {
                $this->$campo = null;
            }
            
        }
    }

    //id_proc_doc : id_processo ou id_documento da tramitacação  
    function SELECT_HISTORICO($id_proc_doc, $tipo)
    {
        global $db;       
        
        $sql = "SELECT id_{$tipo},
                 (select  nome from setor where id = t.id_setor_origem) as origem,
                    (select  nome from setor where id = t.id_setor_destino) as destino, 
                        usuario_tramitou as remetente, usuario_interessado as interessado, data as data_envio
                                from tramitacao_{$tipo} t WHERE id_{$tipo} = {$id_proc_doc}";
        
        $result = $db->Execute($sql);
        
        if ($result === false)
            imprimirSaida("erro", "Falhou na consulta. Tabela tramitacao", false);
        

        $array_historico  = array();
        
        while (!$result->EOF) {
            $tramitacao = new Historico_Tramitacao(); 
            $tramitacao->setarDados($result);                   
            array_push($array_historico, $tramitacao);
                    
            
            $result->MoveNext();
        }
        
        return $array_historico;
    }
    
}
