<?php

include_once "ADO.php";

class Tramitacao_Documento extends ADO
{

    public $id;
    public $id_documento;
    public $id_setor_origem;
    public $id_setor_destino;
    public $usuario_tramitou;
    public $usuario_interessado;
    public $data;
    public $recebido;
    public $usuario_recebeu;

    // retorna se o campo pode ser mandado em branco ou não
    public function getCampoEmBranco($campo)
    {

        $campos_em_branco = array(
            "usuario_recebeu",
            "recebido",
            "data" //DEFAULT NO BANCO
        );

        if (in_array($campo, $campos_em_branco)) {
            return true;
        }

        return false;

    }
}
