<?php

include_once "ADO.php";

class Documento_Listagem extends ADO
{

    public $id;
    public $titulo;
    public $data_recebimento;
    public $autor;
    public $destinatario;
    public $procedencia;
    public $assinado;
    public $tipo;

}
