<?php
include_once "ADO.php";

class Documento extends ADO
{

    public $id;
    public $titulo;
    public $data_recebimento;
    public $observacoes;
    public $destinatario;
    public $autor;
    public $procedencia;
    public $id_assunto;
    public $id_documento_meio;
    public $id_documento_status;
    public $id_documento_tipo;
    public $id_nivel_acesso;
    public $id_nivel_acesso_legislacao;
    public $id_documento_template;
    public $id_documento_arquivo;
    public $id_documento_fisico;
    public $usuario_assinou;
    public $setor_usuario_assinou;
    public $data_assinatura;
    public $usuario_autenticou;
    public $setor_usuario_autenticou;
    public $data_autenticacao;
    public $tipo_autenticacao;

    public function getCampoEmBranco($campo)
    {

        $campos_em_branco = array(
            "id_documento_template",
            "id_documento_arquivo",
            "id_documento_fisico",
            "id_nivel_acesso_legislacao",
            "observacoes",
            "usuario_assinou",
            "setor_usuario_assinou",
            "data_assinatura",
            "usuario_autenticou",
            "setor_usuario_autenticou",
            "data_autenticacao",
            "tipo_autenticacao",
        );

        if (in_array($campo, $campos_em_branco)) {
            return true;
        }

        return false;

    }

    public function setarDados($result)
    {
        $campos = get_object_vars($this);
        foreach ($campos as $campo => $valor) {

            if (array_key_exists($campo, $result->fields)) {
                if (preg_match('/^data/', $campo)) {
                    if ($campo == "data_assinatura" || $campo == "data_autenticacao") {
                        $result->fields[$campo] = $this->formatarData($result->fields[$campo], 'd/m/Y H:i');
                    } else {
                        $result->fields[$campo] = $this->formatarData($result->fields[$campo]);
                    }

                }
                $this->$campo = $result->fields[$campo];
            } else {
                $this->$campo = null;
            }

        }
    }
    public function SELECT($id)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "SELECT * from busca_documento({$id})";

        $result = $db->Execute($sql);

        if ($result == false) {
            imprimirSaida("erro", "Erro ao buscar o {$classe}", false);
        }

        while (!$result->EOF) {
            $this->setarDados($result);
            $result->MoveNext();
        }

        return true;
    }

    public function SELECT_ALL($login)
    {
        global $db;

        $classe = get_class($this);

        $sql = "SELECT id_setor from usuario_setor WHERE login='{$login}'";

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela $classe.", false);
        }

        $id_setor_usuario = $result->fields['id_setor'];

        $sql = "select * from documento_listagem";

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela $classe.", false);
        }

        $array_documento = array();

        while (!$result->EOF) {
            $documento = new Documento_Listagem();

            $documento->setarDados($result);

            $sql = "select id_setor from usuario_setor WHERE login in ('{$documento->autor}', '{$documento->destinatario}')";

            $result_setor = $db->Execute($sql);

            if ($result_setor === false) {
                imprimirSaida("erro", "Falhou na consulta. Tabela $classe.", false);
            }

            $array_setor = $result_setor->getArray();

            $id_setor_0 = $array_setor[0]['id_setor'];

            if (sizeof($array_setor) > 1) {
                $id_setor_1 = $array_setor[1]['id_setor'];
            }

            // só pode visualizar o documento caso ele seja do setor do autor do documento ou do interessado
            if ($id_setor_usuario == $id_setor_0 || $id_setor_usuario == $id_setor_1) {
                array_push($array_documento, $documento);
            }

            $result->MoveNext();
        }

        return $array_documento;
    }

    public function pode_editar($id_documento)
    {

        global $db;

        $sql = "SELECT 1 from documento_assinatura WHERE id_documento='{$id_documento}'";

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela documento_assinatura.", false);
        }

        if ($result->EOF) {
            return true;
        }

        return false;

    }

    public function SELECT_DOCUMENTOS_POR_PROCESSO($id_processo)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "SELECT * from documento where id IN (select id_documento from processo_documento WHERE id_processo  = '{$id_processo}')";

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela documento.", false);
        }

        $array = array();

        while (!$result->EOF) {
            $documento = new Documento();
            $documento->setarDados($result);
            array_push($array, $documento);

            $result->MoveNext();
        }

        return $array;

    }

    public function SELECT_DOCUMENTOS_SEM_PROCESSO()
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "SELECT * FROM documento WHERE NOT EXISTS ( SELECT 1 FROM processo_documento  WHERE processo_documento.id_documento =
        documento.id )";

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela documento.", false);
        }

        $array = array();

        while (!$result->EOF) {
            $documento = new Documento();
            $documento->setarDados($result);
            array_push($array, $documento);

            $result->MoveNext();
        }

        return $array;

    }
}
