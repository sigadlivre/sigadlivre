<?php

require_once "ADO.php";
require_once "Processo.php";
require_once "Documento.php";
require_once "Documento_Meio.php";

class Processo_Documento extends ADO
{

    public $id;
    public $id_processo;
    public $id_documento;
    public $removido;
    public $data_insercao;
    public $observacoes;
    public $login_insercao;

    public function getCampoEmBranco($campo)
    {

        $campos_em_branco = array(
            "observacoes",
            "removido",
        );

        if (in_array($campo, $campos_em_branco)) {
            return true;
        }

        return false;

    }

    public function INSERT()
    {
        global $db;

        $classe = strtolower(get_class($this));
        $campos = "";
        foreach (get_object_vars($this) as $campo => $valor) {
            if ($campo == "id") {
                continue;
            }

            if (!isset($this->$campo)) {
                continue;
            }

            $campos .= $campo . ",";
        }

        //retirando virgula que fica sobrando
        $campos = substr($campos, 0, -1);

        $valores = "";
        foreach (get_object_vars($this) as $campo => $valor) {
            if (!isset($this->$campo)) {
                continue;
            }

            $valores .= $this->sqlSanitize($this->$campo) . ",";
        }

        $valores = substr($valores, 0, -1);

        $sql = "INSERT INTO $classe ($campos) VALUES ($valores) RETURNING id;";

        $result = $db->Execute($sql);

        if ($result === false) {
            return false;
        }
        // SE NAO DER CERTO, RETORNA FALSO PARA O CONTROLADOR PODER VOLTAR A TRANSAÇÃO

        return $result->fields["id"];
    }

    public function DELETE($id)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "UPDATE $classe SET removido = true WHERE id = '$id';";

        $result = $db->Execute($sql);

        if ($result->RecordCount() == 0) {
            imprimirSaida("erro", "Falhou na exclusão. Tabela $classe.", false);
        }

        return "Deletou o $id da tabela $classe com sucesso!";

    }

    public function verificaMeio()
    {

        $processo = new Processo();
        $documento = new Documento();
        $meio_processo = new Documento_Meio();
        $meio_documento = new Documento_Meio();

        $processo->SELECT($this->id_processo);
        $documento->SELECT($this->id_documento);

        if (!isset($processo->id_meio)) {
            imprimirSaida("erro", "Erro ao encontrar o processo!", false);
        }
        if (!isset($documento->id_documento_meio)) {
            imprimirSaida("erro", "Erro ao encontrar o documento!", false);
        }

        $meio_processo->SELECT($processo->id_meio);
        $meio_documento->SELECT($documento->id_documento_meio);

        if ($meio_processo->nome == "NÃO DIGITAL" && $meio_documento->nome == "DIGITAL") {
            imprimirSaida("erro", "Processos não digitais só aceitam documentos não digitais!", false);

        } else if ($meio_processo->nome == "DIGITAL" && $meio_documento->nome == "NÃO DIGITAL") {
            $id_hibrido = $meio_processo->SELECT_ID_MEIO("HÍBRIDO");

            return $processo->UPDATE_meio($id_hibrido); // retorna true ou false

        }

        return true;
    }

    public function verificaSePodeEditar($id_processo)
    {
        global $db;

        //VERIFICAAR QUEM CRIOU E A QUEM O PROCESSO É INTERESSADO

        $sql = "SELECT usuario_atribuido, id_setor FROM atribuicao_processo WHERE id_processo = {$id_processo} ";

        $result = $db->Execute($sql);

        $setores = array();
        $usuarios_que_podem_editar = array();

        if ($result == false) {
            return false;
        }

        while (!$result->EOF) {

            array_push($usuarios_que_podem_editar, $result->fields["usuario_atribuido"]);
            array_push($setores, $result->fields["id_setor"]);

            $result->MoveNext();
        }

        $setores = array_unique($setores); // retirando chaves repetidas

        $sql = "SELECT login FROM usuario_setor  WHERE id_setor in (";

        foreach ($setores as $setor) {
            $sql .= $setor . ",";
        }

        $sql = substr($sql, 0, -1); // retirando virgula que sobra
        $sql .= ") AND id_papel = (SELECT id FROM papel WHERE nome = 'CHEFE')";

        $result = $db->Execute($sql);

        if ($result == false) {
            return false;
        }

        while (!$result->EOF) {

            array_push($usuarios_que_podem_editar, $result->fields["login"]);

            $result->MoveNext();
        }

        //selecionando usuarios que assinaram algum documento que está no processo;

        $sql = "SELECT  doc.usuario as login FROM processo_documento as proc JOIN documento_assinatura as doc on proc.id_processo = {$id_processo} AND proc.id_documento = doc.id_documento";

        $result = $db->Execute($sql);
        if ($result == false) {
            return false;
        }

        while (!$result->EOF) {

            array_push($usuarios_que_podem_editar, $result->fields["login"]);
            $result->MoveNext();
        }

        $usuarios_que_podem_editar = array_unique($usuarios_que_podem_editar);

        return $usuarios_que_podem_editar;
    }
}
