<?php
include_once "conexaoBD.php";
include_once "utilitarios.php";

class ADO
{

    public function getUsuario()
    {
        $data = json_decode(file_get_contents("php://input"));
        if (isset($data->usuario)) {
            return $data->usuario;
        }

        imprimirSaida("erro", "Usuário indefinido!", false);
    }

    public function lerFormulario()
    {

        $token = $_REQUEST["token"];
        $data = json_decode(file_get_contents("php://input"));
        $json = $data->json;

        foreach (get_object_vars($this) as $campo => $valor) {
            if ($campo == "id") {
                continue;
            }

            //trim para caso a string venha com espaços
            if (!isset($json->$campo) || empty(trim($json->$campo))) {

                //retorna true se o campo puder ser mandado em branco
                if ($this->getCampoEmBranco($campo) == false) {
                    imprimirSaida("erro", "Problema na leitura JSON.\nCampo indefinido: $campo.", false);
                }

            } else {
                //começa com a palavra data -> é preciso validar
                if (preg_match('/^data/', $campo)) {
                    $json->$campo = $this->validarData($json->$campo);
                    if (!($json->$campo)) {
                        imprimirSaida("erro", "$campo inválido(a)!", false);
                    }
                }

                $this->$campo = $json->$campo;
            }

        }
        return $_REQUEST;
    }

    // usado para pegar os dados da requisição quando não há necessidade de mandar o objeto inteiro
    public function getDataRequest()
    {
        $data = json_decode(file_get_contents("php://input"));
        $data = isset($data->json) ? $data->json : imprimirSaida("erro", "Json indefinido", false);

        return $data;
    }

    public function getJSON()
    {
        return json_encode($this);
    }

    public function INSERT()
    {
        global $db;

        $classe = strtolower(get_class($this));
        $campos = "";
        foreach (get_object_vars($this) as $campo => $valor) {
            if ($campo == "id") {
                continue;
            }

            if (!isset($this->$campo)) {
                continue;
            }

            $campos .= $campo . ",";
        }

        //retirando virgula que fica sobrando
        $campos = substr($campos, 0, -1);

        $valores = "";
        foreach (get_object_vars($this) as $campo => $valor) {
            if (!isset($this->$campo)) {
                continue;
            }

            $valores .= $this->sqlSanitize($this->$campo) . ",";
        }

        $valores = substr($valores, 0, -1);

        $sql = "INSERT INTO $classe ($campos) VALUES ($valores) RETURNING id;";

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na inserção. Tabela $classe.", false);
        }

        return $result->fields["id"];
    }

    public function UPDATE($id)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $this->lerFormulario();

        $sql = "UPDATE $classe SET ";

        foreach (get_object_vars($this) as $campo => $valor) {
            if ($campo == "id" || (!isset($this->$campo))) {
                continue;
            }

            $sql .= "$campo =" . $this->sqlSanitize($this->$campo) . ",";

        }

        //retirando a ultima virgula que fica sobrando
        $sql = substr($sql, 0, -1);
        $sql .= " WHERE id = {$id}";

        $result = $db->Execute($sql);

        if ($result == false) {
            imprimirSaida("erro", "Falhou na atualização. Tabela $classe.", false);
        }
        return "Atualizou o $id da tabela $classe com sucesso!";

    }

    public function SELECT($id)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "SELECT * from $classe WHERE id = '$id' LIMIT 1";

        $result = $db->Execute($sql);

        if ($result == false) {
            imprimirSaida("erro", "Erro ao buscar o {$classe}", false);
        }

        while (!$result->EOF) {
            $this->setarDados($result);
            $result->MoveNext();
        }

        return true;
    }

    public function DELETE($id)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "DELETE  from $classe WHERE id = '$id'";

        $result = $db->Execute($sql);

        if ($result == false) {
            imprimirSaida("erro", "Falhou na exclusão. Tabela $classe.", false);
        }

        return "Deletou o $id da tabela $classe com sucesso!";

    }

    public function SELECT_ALL($usuario)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "SELECT * from $classe";

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela $classe.", false);
        }

        $array = array();
        $classe = get_class($this);

        while (!$result->EOF) {
            $objeto = new $classe();
            $objeto->setarDados($result);
            array_push($array, $objeto);

            $result->MoveNext();
        }

        return $array;
    }

    // seleciona as linhas onde o campo possuem determinados valores

    public function SELECT_IN($campo, $valores)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "SELECT * FROM {$classe} WHERE {$campo} IN ( ";

        foreach ($valores as $valor) {
            //sqlSanitize utilizado para aplicar aspas simples
            $sql .= $this->sqlSanitize($valor) . ",";
        }

        $sql = substr($sql, 0, -1);
        $sql .= ");";

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela $classe.", false);
        }

        $array = array();
        $classe = get_class($this);

        while (!$result->EOF) {
            $objeto = new $classe();
            $objeto->setarDados($result);
            array_push($array, $objeto);

            $result->MoveNext();
        }

        return $array;
    }

    public function sqlSanitize($s)
    {
        global $db;
        return $db->qStr($s);
    }

    //utilitarios da classe

    public function verificaSeExiste($id)
    {
        global $db;
        $classe = strtolower(get_class($this));

        $sql = "SELECT 1 from {$classe} WHERE id = {$id}";

        $result = $db->Execute($sql);

        if ($result == false) {
            return false;
        }

        while (!$result->EOF) {
            return true;
        }

        return false;

    }

    public function iniciarTrans()
    {
        global $db;
        $db->beginTrans();
    }

    public function voltarTrans()
    {
        global $db;
        $db->rollbackTrans();
    }

    public function comitarTrans()
    {
        global $db;
        $db->commitTrans();
    }

    public function validarData($data)
    {
        $data = trim($data);

        $matches = array();
        $pattern = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/'; //dd/mm/yyyy
        if (!preg_match($pattern, $data, $matches)) {
            return false;
        }

        // month, day, year
        if (!checkdate($matches[2], $matches[1], $matches[3])) {
            return false;
        }

        //convertendo a data para Y-m-d, formato aceito pelo banco de dados
        $data = str_replace('/', '-', $data);
        $data = date('Y/m/d', strtotime($data));
        return $data;
    }

    public function formatarData($str, $format = 'd/m/Y')
    {
        $data = new DateTime($str);
        return $data->format($format);
    }

    public function setarDados($result)
    {
        $campos = get_object_vars($this);
        foreach ($campos as $campo => $valor) {

            if (array_key_exists($campo, $result->fields)) {
                if (preg_match('/^data/', $campo)) {
                    $result->fields[$campo] = $this->formatarData($result->fields[$campo]);
                }
                $this->$campo = $result->fields[$campo];
            } else {
                $this->$campo = null;
            }

        }
    }
}

/*
ADO db função execute():
-executa qualquer instrução SQL fornecida, se a instrução devolver um conjunto de registro, ele retorna um identificador para os mesmo.
Se não retorna ela retornará true (caso:sucesso) e false (caso:falha)
 */

/*

A função moveNext() move o cursor para o próximo registro em um conjunto de registros a partir da posição moveNext() o cursor for movido com sucesso, a função retornará true. Se o movimento é passado EOF, a função retorna falso.

 */
