<?php

include_once "ADO.php";

class Nup extends ADO
{

    public $id;
    public $tipo;
    public $id_processo;
    public $id_documento;

    public function INSERT()
    {
        global $db;

        $classe = strtolower(get_class($this));
        $campos = "";
        foreach (get_object_vars($this) as $campo => $valor) {
            if ($campo == "id") {
                continue;
            }

            if (!isset($this->$campo)) {
                continue;
            }

            $campos .= $campo . ",";
        }

        //retirando virgula que fica sobrando
        $campos = substr($campos, 0, -1);

        $valores = "";
        foreach (get_object_vars($this) as $campo => $valor) {
            if (!isset($this->$campo)) {
                continue;
            }

            $valores .= $this->sqlSanitize($this->$campo) . ",";
        }

        $valores = substr($valores, 0, -1);

        $sql = "INSERT INTO $classe ($campos) VALUES ($valores) RETURNING LPAD (CAST(id AS VARCHAR), 8, '0') AS id;";

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na inserção. Tabela $classe.", false);
        }

        return $result->fields["id"];
    }

    public function criar_nup($id, $tipo)
    {

        if ($tipo == "PROCESSO") {
            $this->id_processo = $id;

        } else if ($tipo == "DOCUMENTO") {
            $this->id_documento = $id;
        }

        $this->tipo = $tipo;

        return $this->INSERT();

    }

    public function gerarNup($id_PD, $ano, $tipo)
    {

        //idPD : id de processo ou documento
        $id_nup = $this->criar_nup($id_PD, $tipo);

        $siorg = "0000827"; // código siorg ufms
        $numero_base = $siorg . $id_nup . $ano;

        return $siorg . "." . $id_nup . "/" . $ano . "-" . $this->digVer($numero_base);

    }

    public function digVer($numero_base)
    {

        //removendo todos caracteres que não são números atraves de expressão regular
        $numero_base = preg_replace("/[^0-9]/", "", $numero_base);
        $lenght = strlen($numero_base);

        if ($lenght != 19) {
            imprimirSaida("erro", "Número base do nup inválido!", false);
        }

        $parte1 = substr($numero_base, 0, 12); //pegando os doze primeiros digitos, retorna de 0-11
        $parte2 = substr($numero_base, 12); //retorna digito 12 para frente
        $parte2 = $parte2 . "00"; //adicionando 2 zeros na parte final do nup

        //intval retorna o valor inteiro da variável;
        $resto1 = intval($parte1) % 97;

        //Concatena ao resto da divisão a segunda parte do NUP, depois calcula o resto da divisão inteira
        $concat = $resto1 . $parte2;

        $resto2 = intval($concat) % 97;

        $dv = 98 - $resto2;

        if ($dv < 10) { // se for menor que 0 -> adiciona um 0 na frente
            $dv = "0" . $dv;
        }

        return $dv;
    }

    public function verificaNup($nup)
    {

        $nup = preg_replace("/[^0-9]/", "", $nup);
        $lenght = strlen($nup);

        if ($lenght != 21) {
            return false;
        }

        $numero_base = substr($nup, 0, -2); //removendo os digitos verificadores do nup
        $dv_antigo = substr($nup, -2); // pegando os dois ultimos

        $dv_gerado = $this->digVer($numero_base);

        if ($dv_antigo != $dv_gerado) {
            return false;
        }
        $nup_gerado = $numero_base . $dv_gerado; // gerando o nup novamente

        $resto = $nup_gerado % 97;

        if ($resto == 1) {
            return true;
        }

        return false;

    }

}
