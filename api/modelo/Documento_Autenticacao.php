<?php

include_once("ADO.php");

class Documento_Autenticacao extends ADO {

    public $id;
    public $usuario;
    public $id_setor;
    public $id_documento;
    public $data;
    public $tipo;

    function getCampoEmBranco($campo){

        $campos_em_branco = array(
            "data", // DEFAULT NO BANCO
        );

        if (in_array($campo, $campos_em_branco)){
                return true;
        }

        return false;

    }
}