<?php

include_once "ADO.php";

class Documento_Meio extends ADO
{

    public $id;
    public $nome;
    public $descricao;

    public function SELECT_ID_MEIO($nome)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "SELECT id FROM $classe WHERE nome = '$nome';";

        $result = $db->Execute($sql);

        if ($result->RecordCount() == 0) {
            return false;
        }

        return $result->fields["id"];

    }
}
