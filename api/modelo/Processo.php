<?php

include_once "ADO.php";

class Processo extends ADO
{

    public $id;
    public $nup;
    public $titulo;
    public $data_recebimento;
    public $id_assunto;
    public $interessado;
    public $id_meio;
    public $id_nivel_acesso;
    public $id_nivel_acesso_legislacao;
    public $autor;
    public $procedencia;

    public function getCampoEmBranco($campo)
    {

        $campos_em_branco = array(
            "nup",
            "id_nivel_acesso_legislacao",
        );

        if (in_array($campo, $campos_em_branco)) {
            return true;
        }

        return false;

    }

    public function updateNUP()
    {

        global $db;

        $sql = "UPDATE processo SET nup = '$this->nup' WHERE id = '$this->id'";
        $result = $db->Execute($sql);

        if ($result == false) {
            return false;
        }
        return true;

    }

    public function UPDATE_MEIO($id_meio)
    {
        global $db;

        $sql = "UPDATE processo SET id_meio= '$id_meio' WHERE id = '$this->id';";

        $result = $db->Execute($sql);
        if ($result == false) {
            return false;
        }

        return true;
    }

    public function SELECT_MEUS_PROCESSOS($login, $limit)
    {

        global $db;

        $sql = "SELECT p.id, p.titulo,  p.interessado, p.autor, s.nome as procedencia, p.nup
        FROM processo p, setor s
        WHERE  p.procedencia = s.id AND  p.id IN
            (SELECT p.id
             FROM processo p
             JOIN atribuicao_processo atp ON (p.id = atp.id_processo
                                              AND atp.usuario_atribuido = '{$login}')
             OR (p.interessado = '{$login}'
                 OR p.autor = '{$login}')
             UNION
               (SELECT id_processo AS id
                FROM processo_documento
                WHERE processo_documento.login_insercao = '{$login}'
                  OR processo_documento.id_documento IN
                    (SELECT id_documento
                     FROM documento_assinatura
                     WHERE usuario = '{$login}') )) ORDER BY data_recebimento DESC ";

        if (isset($limit)) {
            $sql .= "LIMIT {$limit}";
        }
        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela processo.", false);
        }

        $campos = get_object_vars($this);
        $array_processo = array();

        while (!$result->EOF) {
            $processo = new Processo();

            $processo->setarDados($result);

            array_push($array_processo, $processo);

            $result->MoveNext();
        }

        return $array_processo;

    }

    public function SELECT_ALL($login, $limit)
    {
        global $db;

        //   $sql = "SELECT * from processo ORDER BY data_recebimento DESC";

        $sql = "select * from processo_listagem";
        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela processo.", false);
        }

        $campos = get_object_vars($this);
        $array_processo = array();

        while (!$result->EOF) {
            $processo = new Processo();

            $processo->setarDados($result);

            $sql = "select  t.id_setor_destino from tramitacao_processo t where  t.id_processo = {$processo->id} order by t.data desc limit 1";
            $result_2 = $db->Execute($sql);

            if ($result_2 === false) {
                imprimirSaida("erro", "Falhou na consulta. Tabela processo", false);
            }

            if (isset($result_2->fields["id_setor_destino"])) { // o processo foi tramitado

                //  echo "id_processo {$processo->id} destino : {$result_2->fields["id_setor_destino"]}" .PHP_EOL;
                $sql = "select 1 as x from usuario_setor u where u.login = '{$login}'
                     AND u.id_setor = {$result_2->fields['id_setor_destino']}";

            } else { // o processo não foi tramitado ainda
                //  echo "id_processo {$processo->id}" . PHP_EOL;

                $sql = "select 1 as x from usuario_setor u where u.login = '{$login}'
                    AND (u.id_setor = (select id_setor from usuario_setor  where login = '{$processo->autor}')
                    OR u.id_setor = (select id_setor from usuario_setor  where login = '{$processo->interessado}'))";
            }

            // executa o sql se alguma das condições forem verdadeiras a coluna x vai ser retornada
            // se não, o result não retorna nada
            $result_2 = $db->Execute($sql);

            if (isset($result_2->fields["x"])) {
                array_push($array_processo, $processo);
            }

            $result->MoveNext();
        }

        if (isset($limit)) {
            $array_processo = array_slice($array_processo, 0, $limit);
        }

        return $array_processo;
    }
}
