<?php

include_once "ADO.php";

class Processos_Relacionados extends ADO
{
    public $id_processo;
    public $id_processo_relacionado;

    public function SELECT_IN($campo, $valor)
    {
        global $db;

        $classe = strtolower(get_class($this));

        $sql = "SELECT id_processo_relacionado FROM {$classe} WHERE {$campo} = $valor ";
        // ex : select id_processo_relacionado from processos_relacionados where id_processo = 1;

        $result = $db->Execute($sql);

        if ($result === false) {
            imprimirSaida("erro", "Falhou na consulta. Tabela $classe.", false);
        }

        $array = array();

        while (!$result->EOF) {
            // id dos processos relacionados a um determinado processo
            array_push($array, $result->fields["id_processo_relacionado"]);

            $result->MoveNext();
        }

        return $array;

    }

    //seleciona os processo que não estão relacionados a nenhum outro processo
    public function SELECT_PROCS_N_RELACIONADOS()
    {

    }
}
