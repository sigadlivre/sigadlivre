<?php
    $titulo = "Documentos";
    $descricao = "Lista de documentos que possam lhe interessar.";
    $assunto = "documento";
    $operacao = "listar";
?>

<?php include 'parciais/var.php';?>
<?php include 'parciais/head.php';?>
<?php include 'parciais/topo-mobile.php';?>
<?php include 'parciais/menu-lateral.php';?>
<?php include 'parciais/topo.php';?>

<!-- Conteúdo -->
<div class="container-fluid">
    <div class="row">                        
        <div class="col-sm-7">
            <h2>Documentos</h2>
            <p class="info">
                Todos os documentos criados por você ou que foram endereçados a você.
            <p>
        </div>
        <div class="col-sm-5 info">
            <a class="au-btn au-btn-icon btn-primary float-right" href="criar-documento.php">
                <i class="fa fa-plus-circle"></i> Novo documento
            </a>                        
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="todos-documentos">
                <?php include 'parciais/tabela-documentos.php';?>
            </div>
        </div>
    </div>
</div>
<!-- Fim do Conteúdo -->

<?php include 'parciais/rodape.php';?>
<?php include 'parciais/scripts.php'?>
<?php include 'controlador/documento.php'?>

<script type="text/javascript">
    $(listaDocumento());
    $(listaUsuario());
</script>

    </body>
</html>