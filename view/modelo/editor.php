<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Exemplo Template</title>
    <script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/balloon/ckeditor.js"></script>
</head>
<body>
    <img src="cabecalho.png" width="800px">
    <div id="editor" style="border: solid 1px; width: 800px; height: 500px;">
        <?php echo file_get_contents("oficio-template.html"); ?>
    </div>
    <input type="button" value="Salvar" onclick="save()">
    <script>
        BalloonEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );

	function save(){
		alert(document.getElementById('editor').innerHTML);
	}
    </script>
</body>
</html>
