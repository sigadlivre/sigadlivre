<?php
    $titulo = "Criar processo";
    $descricao = "Crie um novo processo.";
    $assunto = "processo";
    $operacao = "criar";
?>

<?php include 'parciais/var.php';?>
<?php include 'parciais/head.php';?>
<?php include 'parciais/topo-mobile.php';?>
<?php include 'parciais/menu-lateral.php';?>
<?php include 'parciais/topo.php';?>

<!-- Conteúdo -->
<div class="container-fluid">
    <div class="row topo-view">                        
            <div class="col-sm-12 form-group">
                <h2>Novo processo</h2>
                <p class="info">
                    Preencha os campos abaixo atentamente para criar um novo processo.
                <p>
            </div>
    </div>
    <?php include 'parciais/formulario-processo.php';?>
</div>
<!-- Fim do Conteúdo -->

<?php include 'parciais/rodape.php';?>
<?php include 'parciais/scripts.php'?>
<?php include 'controlador/processo.php'?>
<?php include 'controlador/usuario.php';?>

    <script type="text/javascript">
        recuperaAssuntos();
        recuperaTipoDocumento();
        recuperaSetores();
        recuperaNiveisDeAcesso();
        recuperaUsuarios();
    </script>
    
	</body>
</html>