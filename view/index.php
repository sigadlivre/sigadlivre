<?php
    $titulo = "Bem vindo ao SIGAD!";
    $descricao = "Bem vindo ao SIGAD!";
    $assunto = "inicio";
    $operacao = "listar";
?>

<?php include 'parciais/var.php';?>
<?php include 'parciais/head.php';?>
<?php include 'parciais/topo-mobile.php';?>
<?php include 'parciais/menu-lateral.php';?>
<?php include 'parciais/topo.php';?>

<!-- Conteúdo -->
<div class="container">
	<div class="row">                        
	    <div class="col-sm-7">
	        <h1 class="title-1 saudacao">Bem vindo ao SIGAD Livre!</h1>
	        <p class="info">Veja e gerencie processos e documentos.</p>
	    </div>
	    <div class="col-sm-5 info">
	        <a class="au-btn au-btn-icon btn-primary float-right" href="criar-processo.php">
	            <i class="fa fa-plus-circle"></i> Novo processo
	        </a>	        	        
	    </div>
	</div>

	<div class="row processos-setor">
		<h3 class="info">Últimos processos do setor</h3>
	    <div class="col-sm-12">
	        <div class="todos-processos">
	        	<?php include 'parciais/tabela-processos.php';?>
	        </div>
	    </div>
	</div>

	<div class="row processos-usuario">
		<h3 class="info">Seus últimos processos</h3>
	    <div class="col-sm-12">
	        <div class="meus-processos">
	        	<?php include 'parciais/tabela-processos.php';?>
	        </div>
	    </div>
	</div>
</div>
<!-- Fim do Conteúdo -->

<?php include 'parciais/rodape.php';?>
<?php include 'parciais/scripts.php'?>
<?php include 'controlador/processo.php';?>

	<script type="text/javascript">
		listaProcessos();
		listaMeusProcessos()
	</script>

    </body>
</html>