<?php
    $titulo = "Processos";
    $descricao = "Lista de processos que possam lhe interessar.";
    $assunto = "processo";
    $operacao = "listar";
?>

<?php include 'parciais/var.php';?>
<?php include 'parciais/head.php';?>
<?php include 'parciais/topo-mobile.php';?>
<?php include 'parciais/menu-lateral.php';?>
<?php include 'parciais/topo.php';?>

<!-- Conteúdo -->
<div class="container-fluid">
    <div class="row">                        
        <div class="col-sm-7">
            <h2>Todos os processos do setor</h2>
            <p class="info">
                Todos os processos do seu setor.
            <p>
        </div>
        <div class="col-sm-5 info">
            <a class="au-btn au-btn-icon btn-primary float-right" href="criar-processo.php">
                <i class="fa fa-plus-circle"></i> Novo processo
            </a>                        
        </div>
    </div>    
    <div class="row">
        <div class="col-sm-12">
            <div class="todos-processos">
                <?php include 'parciais/tabela-processos.php';?>
            </div>
        </div>
    </div>
</div>
<!-- Fim do Conteúdo -->

<?php include 'parciais/rodape.php';?>
<?php include 'parciais/scripts.php'?>
<?php include 'controlador/processo.php';?>
    <script type="text/javascript">
        listaProcessos();
    </script>
    </body>
</html>