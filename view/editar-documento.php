<?php
    $titulo = "Detalhes do documento";
    $descricao = "Detalhes do documento.";
    $assunto = "documento";
    $operacao ="alterar";
?>

<?php include 'parciais/var.php';?>
<?php include 'parciais/head.php';?>
<?php include 'parciais/topo-mobile.php';?>
<?php include 'parciais/menu-lateral.php';?>
<?php include 'parciais/topo.php';?>

<!-- Conteúdo -->
<div class="container-fluid">
    <div class="row topo-view">                        
        <div class="col-sm-7 form-group">
            <h2>Editar documento</h2>
            <p class="info">
                Veja e edite os detalhes do documento.
            <p>
        </div>
        <div class="col-sm-5 info">
            <button class="btn-enviar au-btn au-btn-icon btn-primary float-right" data-toggle="modal" data-target="#enviarDocumento">
                <i class="fas fa-paper-plane"></i> Enviar documento
            </button>                       
        </div>
    </div>
    <div class="default-tab">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="editor-documento-tab" data-toggle="tab" href="#editor-documento" role="tab" aria-controls="editor-documento" aria-selected="true">
                    <i class="far fa-file"></i> Documento
                </a>
                <a class="nav-item nav-link" id="documentos-tab" data-toggle="tab" href="#documentos" role="tab" aria-controls="documentos"  aria-selected="false">
                    <i class="fas fa-align-left"></i> Detalhes do documento
                </a>
                <a class="nav-item nav-link" id="historico-tab" data-toggle="tab" href="#historico" role="tab" aria-controls="historico"     aria-selected="false">
                    <i class="fas fa-history"></i> Histórico de tramitação
                </a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <!-- Visualização do documento -->
            <div class="tab-pane fade show active container" id="editor-documento" role="tabpanel" aria-labelledby="editor-documento-tab">
                <div class="row">                        
                    <?php include 'parciais/editor.php';?>
                </div>
                <div class="row">
                    <div class="col-sm-8 offset-sm-2">
                        <button class="au-btn au-btn-icon btn btn-success btn-sm" onclick="assina()">
                            <i class="fas fa-pen-nib"></i> Assinar
                        </button>

                        <button class="au-btn au-btn-icon btn btn-primary btn-sm" onclick="save()">
                            <i class="fas fa-save"></i> Salvar
                        </button>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-sm-8 offset-sm-2">
                        <p class="info">
                            *Uma que vez assinado o documento não pode ser alterado.
                        </p>
                    </div>
                </div>
                
            </div>
            <!-- Fim da Visualização do documento  -->

            <!-- Meta dados do documento -->
            <div class="tab-pane fade container documentos-tab" id="documentos" role="tabpanel" aria-labelledby="documentos-tab">                            
                <?php include 'parciais/formulario-documento.php';?>
            </div>
            <!-- Fim dos Meta dados do documento -->

            <!-- Tabela de historico -->
            <div class="tab-pane fade container historico-tab" id="historico" role="tabpanel" aria-labelledby="historico-tab">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Histórico de Tramitação</h3>
                    </div>
                </div>
                <?php include "parciais/tabela-historico.php";?>
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn-enviar au-btn au-btn-icon btn-primary" data-toggle="modal" data-target="#enviarDocumento">
                        <i class="fas fa-paper-plane"></i> Enviar documento
                    </button> 
                    </div>
                </div>
            </div>
            <!-- Fim do histórico -->

        </div>
    </div>    
</div>
<!-- Fim do Conteúdo -->

<?php include 'parciais/rodape.php';?>
<?php include 'parciais/scripts.php';?>
<?php include 'controlador/documento.php';?>
<?php include 'controlador/usuario.php';?>

<script type="text/javascript">    
    $(exibeDocumento());
    $(recuperaUsuarios());
    $(recuperaHistorico());
</script>

	</body>
</html>