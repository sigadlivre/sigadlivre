<?php
    $titulo = "Processo".$processo->titulo;
    $descricao = "Visualizar detalhes do processo #".$processo->nup;
    $operacao = "buscar";
?>

<?php include 'parciais/var.php';?>
<?php include 'parciais/head.php';?>
<?php include 'parciais/topo-mobile.php';?>
<?php include 'parciais/menu-lateral.php';?>
<?php include 'parciais/topo.php';?>

<!-- Conteúdo -->
	<div class="container">
		<div class="row topo-view">
			<div class="col-sm-6">
	            <h2>Editar processo</h2>
	            <p class="info">
                    Edite o processo assim como os seus documentos anexados
                <p>
	        </div>
	        <div class="col-sm-6 info">
                <button class="btn-enviar au-btn au-btn-icon btn-primary float-right" data-toggle="modal" data-target="#enviarProcesso">
	            	<i class="fas fa-paper-plane"></i> Enviar processo
	            </button>
	            <div class="dropdown show">
				  <a class="au-btn au-btn-icon btn-secondary float-right dropdown-toggle" href="#" role="button" id="menuProcesso" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fas fa-bars"></i> Opções
				  </a>

				  <div class="dropdown-menu" aria-labelledby="menuProcesso">
				    <a class="dropdown-item" href="#">
	                    <i class="fas fa-edit"></i> Editar processo
	                </a>
	                <a class="dropdown-item" href="adicionar-documento.php?id=<?php echo intval($_GET['id']);?>">
		            	<i class="fa fa-file"></i> Adicionar documentos
		            </a>
				    <button class="dropdown-item" data-toggle="modal" data-target="#relacionarProcesso"><i class="fas fa-link"></i> Relacionar processo</button>
				  </div>
				</div>                    
            </div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="default-tab">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="processo-tab" data-toggle="tab" href="#processo" role="tab" aria-controls="processo" aria-selected="true">
								<i class="fas fa-align-left"></i> Dados do processo
							</a>
							<a class="nav-item nav-link" id="documentos-tab" data-toggle="tab" href="#documentos" role="tab" aria-controls="documentos"	 aria-selected="false">
								<i class="far fa-file"></i> Documentos
							</a>
							<a class="nav-item nav-link" id="historico-tab" data-toggle="tab" href="#historico" role="tab" aria-controls="historico"	 aria-selected="false">
								<i class="fas fa-history"></i> Histórico de tramitação
							</a>
							<a class="nav-item nav-link" id="relacionados-tab" data-toggle="tab" href="#relacionados" role="tab" aria-controls="relacionados"	 aria-selected="false">
								<i class="fas fa-archive"></i> Processos relacionados
							</a>

						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<!-- Dados do processo -->
						<div class="tab-pane fade show active container" id="processo" role="tabpanel" aria-labelledby="processo-tab">
							<div class="row">                        
						            <div class="col-sm-12 form-group">
						                <p class="info">
						                    Visualize os detalhes do processo e se necessário edite-os.
						                <p>
						            </div>
						    </div>
							<?php include 'parciais/formulario-processo.php';?>
						</div>
						<!-- Fim dos Dados do processo -->

						<!-- Tabela de documentos -->
						<div class="tab-pane fade container documentos-tab" id="documentos" role="tabpanel" aria-labelledby="documentos-tab">
							<div class="row">
								<div class="col-sm-12">
									<h3>Volume 1</h3>
									<table class="table container table-borderless table-striped table-earning tabela-documentos">
					                    <thead>
					                        <tr class="row">
					                            <th class="col-sm-2">Tipo</th>
					                            <th class="col-sm-2">Autor</th>
					                            <th class="col-sm-2">Origem</th>
					                            <th class="col-sm-2">Criação</th>
					                            <th class="col-sm-2">Assinado</th>
					                            <th class="col-sm-2">Ações</th>
					                        </tr>
					                    </thead>
					                    <tbody id="documentos-processo">
					                    </tbody>
					                </table>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 info">
									<a class="au-btn au-btn-icon btn-primary" href="adicionar-documento.php?id=<?php echo intval($_GET['id']);?>">
								    	<i class="fa fa-file"></i> Adicionar documentos
								    </a>
								</div>
							</div>
							
						</div>
						<!-- Fim da Lista ordenada de volumes e documentos -->

						<!-- Tabela de historico -->
						<div class="tab-pane fade container historico-tab" id="historico" role="tabpanel" aria-labelledby="historico-tab">
							<div class="row">
								<div class="col-sm-12">
									<h3>Histórico de Tramitação</h3>
								</div>
							</div>
							<?php include "parciais/tabela-historico.php";?>
			                <div class="row">
			                	<div class="col-12 info">
				                	<button class="btn-enviar au-btn au-btn-icon btn-primary" data-toggle="modal" data-target="#enviarProcesso">
						            	<i class="fas fa-paper-plane"></i> Enviar processo
						            </button>
				                </div>
			                </div>
						</div>
						<!-- Fim do histórico -->

						<!-- Tabela de processos relacionados -->
						<div class="tab-pane fade container relacionados-tab" id="relacionados" role="tabpanel" aria-labelledby="relacionados-tab">
							<div class="row">
								<div class="col-sm-12 processos-relacionados">
									<?php include 'parciais/tabela-processos.php';?>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 info">
									<button class="btn-enviar au-btn au-btn-icon btn-primary" data-toggle="modal" data-target="#relacionarProcesso">
						            	<i class="fas fa-link"></i> Relacionar processos
						            </button>
								</div>
							</div>
							
						</div>
						<!-- Fim da Lista de processos relacionados -->

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal tramitação -->
	<div class="modal fade" id="enviarProcesso" tabindex="-1" role="dialog" aria-labelledby="enviarProcessoLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="enviarProcessoLabel">Novo documento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php include 'parciais/formulario-tramitacao.php';?>
                </div>                      
            </div>
        </div>
    </div>


<!-- Fim do Conteúdo -->

<?php include 'parciais/rodape.php';?>
<?php include 'parciais/scripts.php';?>
	<?php include 'controlador/documento.php';?>
	<?php include 'controlador/processo.php';?>	
	<?php include 'controlador/usuario.php';?>
    <script type="text/javascript">
        exibeProcesso();
        listaDocumentosPorProcesso();
        recuperaHistorico();
        recuperaSetores();
        recuperaUsuarios();
    </script>
    </body>
</html>