<script>

    //Definição das variaveis que serão utilizadas nas requisições
    var api = "<?php echo $api; ?>";
    var token = "lsdfjklsdfjsdlfjsdlfsdfsdf";
    var operacao = "<?php echo $operacao; ?>";

    //Define o usuario que sera enviado em cada requisição
    var usuario = new Object();
    usuario.login = "<?php echo $login; ?>";

    //Cria o objeto de dados que deve ser passado obrigatóriamente em cada requisição
    var data = new Object();
    data.usuario = usuario;

    var id_setor = "<?php echo $id_setor; ?>";

    //Recupera o id do processo via GET, caso exista
    var id = "<?php echo intval($_GET['id']);?>";

    function salvaProcesso(){
        operacao = "alterar";

        data.json = $("#form-processo").serializeFormJSON();

        console.log(JSON.stringify(data.json));

        $.post(api+"controlador_geral.php?token="+token+"&operacao="+operacao+"&controlador=Processo&id="+id, JSON.stringify(data))
            .always(function( resposta ){
                if(resposta["success"]){
                    alert("Processo Salvo");
                }
            });
    }

    //Recupera o historico de tramitacao do processo
    function recuperaHistorico(){

        $.post(api+"processo.php?token="+token+"&operacao=historico_tramitacao&id="+id,JSON.stringify(data))
        .always(function( resposta ){

            if(resposta['success']){
                console.log()
                var envios = resposta["historico_tramitacao"];

                for(var envio in envios) {

                    $(".tabela-historico tbody").append(""+
                    "<tr class=\"row\">"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Origem: </span>"+envios[envio].origem+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Destino: </span>"+envios[envio].destino+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Remetente: </span>"+envios[envio].remetente+"</td>"+
                    "<td class=\"col-sm-3\"><span class=\"d-inline d-sm-none\">Interessado: </span>"+envios[envio].interessado+"</td>"+
                    "<td class=\"col-sm-3\"><span class=\"d-inline d-sm-none\">NUP: </span>"+envios[envio].data_envio+"</td>"+
                    "</tr>");
                   
                }
            }
        });

    }

    //Relaciona o id de determinado processo ao processo atual
    function relaciona(id_processo_relacionado){

        data.json = new Object;

        data.json.id_processo = id;
        data.json.id_processo_relacionado = id_processo_relacionado;

        $.post(api+"processo.php?token="+token+"&operacao=relacionar_processo&id="+id,JSON.stringify(data))
        .always(function( resposta ){
            console.log(resposta);
        });
    }

    

    //Envia o processo ao Setor, interessado- Tramitação
    function envia(){
        data.json = $("#form-tramitacao").serializeFormJSON();
        data.json.id_setor_destino = data.json.procedencia;

        data.json.id_processo = id;
        data.json.id_setor_origem = id_setor;
        data.json.usuario_tramitou = usuario.login;
        data.json.recebido = true;
        data.json.data = recuperaData(); 


        console.log(JSON.stringify(data));

        $("#form-tramitacao").submit(function(e) {
            e.preventDefault();
        });

        $.post(api+"processo.php?token="+token+"&operacao=tramitar", JSON.stringify(data))
            .always(function( resposta ){
                if(resposta["success"]){
                    alert("Tramitação salva!");
                    window.location.replace("processos.php");
                }
            });
    }

    
    //Serializa formulário em JSON
    (function ($) {
        $.fn.serializeFormJSON = function () {

            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);

    
    $('#form-processo').submit(function (e) {
        e.preventDefault();
        data.json = $(this).serializeFormJSON();

        if(operacao == "criar")
            criaProcesso(data);
        if(operacao == "buscar")
            salvaProcesso(data);
    });

    //Cria processo
    function criaProcesso(data){

        $.post(api+"processo.php?token="+token+"&operacao="+operacao, JSON.stringify(data))
        .done(function( resposta ) {
            if(resposta["success"])
                window.location.replace("detalhes-processo.php?id="+resposta["id"]);
            else
                alert("Erro ao criar processo entre em contato como administrador");
        });
    }

    //Desabilita edição
    function apenasLeituraProcesso(){
        $('#form-processo :input').attr('disabled', true);
    }

    //Exibe a lista de leis caso o processo não seja publico
    function exibeLegislacao(exibir) {
        if (exibir == true) {
            document.getElementById("legislacao").style.display = "block";
            document.getElementById("legislacaoCampo").required = true;
        } else {
            document.getElementById("legislacao").style.display = "none";
            document.getElementById("legislacaoCampo").required = false;
        }
    }

    //Exibe as opções adequadas para o meio selecionado
    function selecionaMeio(meio) {

        document.getElementById("meioHibrido").style.display = "none";
        document.getElementById("meioHibridoCampo").required = false;
       
        document.getElementById("meioFisico").style.display = "none";
        document.getElementById("meioFisicoCampo").required = false;

        if (meio.value == '1') {
            document.getElementById("meioDigital").style.display = "block";
            document.getElementById("meioDigitalCampo").required = true;
        } else  if (meio.value == '2') {
            document.getElementById("meioHibrido").style.display = "block";
            document.getElementById("meioHibridoCampo").required = true;
        }else  if (meio.value == '3') {
            document.getElementById("meioFisico").style.display = "block";
            document.getElementById("meioFisicoCampo").required = true;
        }
    }

    //Detalhes do processo
    function opcoesMeio(resposta){
        var opcoes = "";

        if(resposta["id_meio"] == "1"){
            opcoes += "<div class=\"col-sm-9 form-group\" id=\"meioHibrido\" style=\"display: none\">";
            opcoes += "<label class=\"control-label\" for=\"arquivo_processo\">Envie o arquivo do processo:</label>";
            opcoes += "<input  id=\"meioHibridoCampo\" class=\"form-control\" type=\"file\" accept=\"application/pdf\" name=\"arquivo_processo\">";
            opcoes += "</div>";
        }else if(resposta["id_meio"] == "2"){
            opcoes += "<div class=\"col-sm-9 form-group\" id=\"meioFisico\" style=\"display: none\">"; 
            opcoes += "<label class=\"control-label\" for=\"localização\">Localização do processo:</label>";
            opcoes += "<input  id=\"meioFisicoCampo\" class=\"form-control\" type=\"text\" name=\"localização\">";
            opcoes += "</div>";
        }

        return opcoes;
    }

    function exibeNivelDeAcesso(id){
        var opcoes = "";

        if(id == "1"){
            opcoes += "<div class=\"nivel-acesso\"><input  class=\"form-check-input \" onchange=\"exibeLegislacao(false)\" type=\"radio\" name=\"id_nivel_acesso\" value=\"1\">Público</div>";
            opcoes += "<div class=\"nivel-acesso\"><input  class=\"form-check-input \" onchange=\"exibeLegislacao(true)\" type=\"radio\" name=\"id_nivel_acesso\" value=\"2\">Sigiloso</div>";
        }else if(id == "2"){
            opcoes += "<div class=\"nivel-acesso\"><input  class=\"form-check-input \" onchange=\"exibeLegislacao(false)\" checked type=\"radio\" name=\"id_nivel_acesso\" value=\"1\">Público</div>";
            opcoes += "<div class=\"nivel-acesso\"><input  class=\"form-check-input \" onchange=\"exibeLegislacao(true)\" checked type=\"radio\" name=\"id_nivel_acesso\" value=\"2\">Sigiloso</div>";
        }else{
            opcoes += "<div class=\"nivel-acesso\"><input  class=\"form-check-input \" onchange=\"exibeLegislacao(false)\" checked type=\"radio\" name=\"id_nivel_acesso\" value=\"1\">Público</div>";
            opcoes += "<div class=\"nivel-acesso\"><input  class=\"form-check-input \" onchange=\"exibeLegislacao(true)\" type=\"radio\" name=\"id_nivel_acesso\" value=\"2\">Sigiloso</div>";
        }

        return opcoes;
    }        

    //Exibe a lista de leis caso o processo não seja publico
    function exibeLegislacao(exibir) {
        if (exibir == true) {
            document.getElementById("legislacao").style.display = "block";
            document.getElementById("legislacaoCampo").required = true;
        } else {
            document.getElementById("legislacao").style.display = "none";
            document.getElementById("legislacaoCampo").required = false;
        }
    }

    
    //Exibe os detalhes do processo
    function exibeProcesso(){
        $.post(api+"controlador_geral.php?token="+token+"&operacao="+operacao+"&controlador=Processo&id="+id, JSON.stringify(data))
            .always(function( resposta ){

                if(resposta['success']){
                    resposta = resposta["processo"];

                    console.log(resposta)

                    for(campo in resposta){

                        recuperaAssuntos();
                        recuperaNiveisDeAcesso();

                        $("input[name='"+campo+"']").val(resposta[campo]);
                        $("textarea[name='"+campo+"']").val(resposta[campo]);

                        $('select[name=procedencia] option[value=2]').prop('selected', true);
                        escolheNivelAcesso(resposta["id_nivel_acesso"]);
                    }
                }
            });
        }

    //Lista processos não relacionados
    function listaNaoRelacionado(){
        $.post(api+"controlador_geral.php?token="+token+"&operacao=listar&controlador=Processo&limit=10", JSON.stringify(data))
        .always(function( resposta ){            if(resposta['success']){
                resposta = resposta["processos"];
                for(var processo in resposta) {
                    $(".processos-nao-relacionados .tabela-processos tbody").append(rowProcesso(resposta[processo]));
                    $(".processos-nao-relacionados .tabela-processos tbody #edit-"+resposta[processo].id).replaceWith(" "+
                    "<button onclick=\"relaciona("+resposta[processo].id+")\" class=\"btn btn-success btn-sm d-block d-sm-inline\"><i class=\"fas fa-link\"></i> Ligar</button>");
                }
            }
        });
    }

    //Listar os processos relacionados
    function listaRelacionados() {

        console.log(JSON.stringify(data));

        $.post(api+"processo.php?token="+token+"&operacao=processos_relacionados&id="+id, JSON.stringify(data))
        .always(function( resposta ){
            if(resposta['success']){
                resposta = resposta["processos_relacionados"];
                for(var processo in resposta) {
                    $(".processos-relacionados .tabela-processos tbody").append(rowProcesso(resposta[processo]));
                }
            }
        });
    }

    //Listar os processos do usuário
    function listaMeusProcessos() {

        console.log(JSON.stringify(data));

        $.post(api+"processo.php?token="+token+"&operacao=meus_processos&limit=10", JSON.stringify(data))
        .always(function( resposta ){

            if(resposta['success']){
                resposta = resposta["processos"];
                for(var processo in resposta) {
                    $(".meus-processos .tabela-processos tbody").append(rowProcesso(resposta[processo]));
                }
            }
        });
    }

    //Listar processos
    function listaProcessos() {

        console.log(JSON.stringify(data));

        $.post(api+"controlador_geral.php?token="+token+"&operacao="+operacao+"&controlador=Processo&limit=10", JSON.stringify(data))
        .always(function( resposta ){
            if(resposta['success']){
                resposta = resposta["processos"];
                for(var processo in resposta) {
                    $(".todos-processos tbody").append(rowProcesso(resposta[processo]));                   
                }
            }
        });
    }

    function rowProcesso(processo){

        return "<tr class=\"row\">"+
                    "<td class=\"col-sm-3\"><span class=\"d-inline d-sm-none\">NUP: </span><small>"+processo.nup+"</small></td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Título:  </span>"+processo.titulo+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Autor:  </span>"+processo.autor+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Origem:  </span>"+processo.procedencia+"</td>"+
                    "<td class=\"col-sm-3 acao-"+processo.id+"\"><a class=\"btn btn-primary btn-sm d-block d-sm-inline\" target=\"_blank\" href=\"detalhes-processo.php?id="+processo.id+"\"><i class=\"fa fa-eye\"></i> Ver</a> <a id=\"edit-"+processo.id+"\" class=\"btn btn-primary btn-sm\" target=\"_blank\" href=\"editar-processo.php?id="+processo.id+"\"><i class=\"fas fa-edit\"></i> Editar</a></td>"+
                "</tr>";
    }

</script>