<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/balloon/ckeditor.js"></script>

<script type="text/javascript">

    //Definição das variaveis que serão utilizadas nas requisições
    var api = "<?php echo $api; ?>";
    var token = "lsdfjklsdfjsdlfjsdlfsdfsdf";
    var operacao = "<?php echo $operacao; ?>";    

    //Define o usuario que sera enviado em cada requisição
    var usuario = new Object();
    usuario.login = "<?php echo $login; ?>";

    var id_setor = "<?php echo $id_setor; ?>";

    //Cria o objeto de dados que deve ser passado obrigatóriamente em cada requisição
    var data = new Object();
    data.usuario = usuario;

    //Recupera o id do processo via GET, caso exista
    var id = "<?php echo intval($_GET['id']);?>";

    function selecionaTipoDocumento(id_documento_tipo){
        if (id_documento_tipo == 1) {
            return "Oficio";
        }
    }

    //Verifica se o documento esta assinado
    function verificaAssinatura(assinado){

        if(assinado === "t")
            return "Assinado";
        else
            return "Não assinado";

    }

    function adicionaDocumento(id_processo,id_documento){

        var data = new Object();
        data.id_documento = id_documento;
        data.id_processo = id_processo;
        data.data_insercao = recuperaData();
        data.login_insercao = "fulano.tal";
        data.observacoes = "";
        console.log(data);

        console.log("<?php echo $api; ?>processo_documento.php?token=lsdfjklsdfjsdlfjsdlfsdfsdf&operacao=criar");

        $.post( "<?php echo $api; ?>processo_documento.php?token=lsdfjklsdfjsdlfjsdlfsdfsdf&operacao=criar", "{\"json\":"+JSON.stringify(data)+"}")
          .always(function( resposta ) {
            console.log(resposta);
          });
    }

    //Exibe a lista de leis caso o documento não seja publico
    function exibeLegislacao(exibir) {
        if (exibir == true) {
            document.getElementById("legislacao").style.display = "block";
            document.getElementById("legislacaoCampo").required = true;
        } else {
            document.getElementById("legislacao").style.display = "none";
            document.getElementById("legislacaoCampo").required = false;
        }
    }

    //Exibe as opções adequadas para o meio selecionado
    function selecionaMeio(meio) {

        document.getElementById("meioHibrido").style.display = "none";
        document.getElementById("meioHibridoCampo").required = false;
       
        document.getElementById("meioFisico").style.display = "none";
        document.getElementById("meioFisicoCampo").required = false;

        if (meio.value == '1') {
            document.getElementById("meioDigital").style.display = "block";
            document.getElementById("meioDigitalCampo").required = true;
        } else  if (meio.value == '2') {
            document.getElementById("meioHibrido").style.display = "block";
            document.getElementById("meioHibridoCampo").required = true;
        }else  if (meio.value == '3') {
            document.getElementById("meioFisico").style.display = "block";
            document.getElementById("meioFisicoCampo").required = true;
        }
    }

    function carregaConteudoDocumento(id_documento_template){

        $.post(api+"controlador_geral.php?token="+token+"&operacao=buscar&controlador=Documento_Template&id="+id_documento_template,JSON.stringify(data))
        .always(function( resposta ){
            if(resposta["success"]){
                resposta = resposta["documento_template"];
                $('.documento').append(resposta["conteudo"]);
                BalloonEditor.create( document.querySelector( '#editor' ) )
                .catch( error => {
                    console.error( error );
                } );
            }
        });
    }



    function carregaTemplateDocumento(id_documento_template){

        $.post(api+"controlador_geral.php?token="+token+"&operacao=buscar&controlador=Template&id="+id_documento_template,JSON.stringify(data))
        .always(function( resposta ){
            if(resposta["success"]){
                resposta = resposta["template"];
                $('.documento').append(resposta["conteudo"]);
                BalloonEditor.create( document.querySelector( '#editor' ) )
                .catch( error => {
                    console.error( error );
                } );

            }
        });
    }

    function selecionaDocumentoStatus(id_status){
        if (id_status == 1) {
            return "Não assinado";
        }else if (id_status == 2) {
            return "Assinado";
        }
    }


    function save(){
        var template = new Object();
        template.conteudo = document.getElementById('editor').innerHTML;
        template.data_criacao = recuperaData();
        template.id_template = "1";

        data.json = template;

        $.post(api+"controlador_geral.php?token="+token+"&controlador=Documento_Template&operacao=criar", JSON.stringify(data))
        .done(function( resposta ) {
            console.log(resposta);
            if(resposta["success"]){
                var json = $('#form-documento').serializeFormJSON();
                json.id_documento_template = resposta["id"];

                console.log(data);
                data.json = json;
                console.log(data);

                $.post(api+"controlador_geral.php?token="+token+"&controlador=Documento&operacao=alterar&id="+id,JSON.stringify(data))
                .always(function( resposta ) {
                    if(resposta["success"]){
                        alert("Documento salvo!");
                    }
                });
            }
        });

    }

    //Transforma o formulário em JSON
    (function ($) {
        $.fn.serializeFormJSON = function () {

            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);

    //Define a operação a ser realizada assim que um formulário é enviado
    $('#form-documento').submit(function (e) {
        e.preventDefault();

        if(operacao=="criar"){
            criaDocumento();
        }else if(operacao=="alterar"){
            atualizaDocumento();
        }
    });

    //Recupera o historico de tramitacao do documento
    function recuperaHistorico(){

        $.post(api+"documento.php?token="+token+"&operacao=historico_tramitacao&id="+id,JSON.stringify(data))
        .always(function( resposta ){

            if(resposta['success']){
                console.log()
                var envios = resposta["historico_tramitacao"];

                for(var envio in envios) {

                    $(".tabela-historico tbody").append(""+
                    "<tr class=\"row\">"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Origem: </span>"+envios[envio].origem+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Destino: </span>"+envios[envio].destino+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Remetente: </span>"+envios[envio].remetente+"</td>"+
                    "<td class=\"col-sm-3\"><span class=\"d-inline d-sm-none\">Interessado: </span>"+envios[envio].interessado+"</td>"+
                    "<td class=\"col-sm-3\"><span class=\"d-inline d-sm-none\">NUP: </span>"+envios[envio].data_envio+"</td>"+
                    "</tr>");
                   
                }
            }
        });

    }

    //Envia o documento ao Setor, interessado- Tramitação
    function envia(){
        data.json = $("#form-tramitacao").serializeFormJSON();
        data.json.id_setor_destino = data.json.procedencia;

        data.json.id_documento = id;
        data.json.id_setor_origem = id_setor;
        data.json.usuario_tramitou = usuario.login;
        data.json.recebido = true;
        data.json.data = recuperaData(); 


        console.log(JSON.stringify(data));

        $("#form-tramitacao").submit(function(e) {
            e.preventDefault();
        });

        $.post(api+"documento.php?token="+token+"&operacao=tramitar", JSON.stringify(data))
            .always(function( resposta ){
                if(resposta["success"]){
                    alert("Tramitação salva!");
                    window.location.replace("documentos.php");
                }
            });
    }

    //Realiza a requisição para criar documentos
    function criaDocumento(){
        var json = $('#form-documento').serializeFormJSON();
        data.json = json;

        var template;

        //data.json["id_documento_template"] = json["id_documento_tipo"];
        
        console.log(JSON.stringify(data));

        $.post(api+"controlador_geral.php?token="+token+"&operacao="+operacao+"&controlador=Documento",JSON.stringify(data))
        .done(function( resposta ) {
            if(resposta["success"]){
                alert( "Documento Criado com sucesso" );

                $.post(api+"controlador_geral.php?token="+token+"&controlador=Template&operacao=buscar&id="+json["id_documento_tipo"], JSON.stringify(data))
                .done(function( respostaTemplate ) {
                    console.log(respostaTemplate);
                    if(respostaTemplate["success"]){
                        template = respostaTemplate["template"];
                        template["data_criacao"] = "2018/01/01";
                        template["id_template"] = json["id_documento_tipo"];

                        
                        data.json = template;
                        delete data.json.id;

                        $.post(api+"controlador_geral.php?token="+token+"&controlador=Documento_Template&operacao=criar", JSON.stringify(data))
                        .done(function( respostaCriarTemplate ) {
                            console.log(data);
                            if(respostaCriarTemplate["success"]){
                                window.location.replace("detalhes-documento.php?id="+resposta["id"]);
                            }
                        });
                    }
                });

            }
        });
        
    }

    //Assina o documento
    function assina(){

        data.json = new Object();
        data.json.usuario = usuario.login;
        data.json.id_setor = id_setor;
        data.json.id_documento = id;
        data.json.data = recuperaData();

        $.post(api+"documento.php?token="+token+"&operacao=assinar",JSON.stringify(data))
        .always(function( resposta ){
            if(resposta["success"]){
                alert("Documento Assinado com sucesso");
            }
        });
    }

    //Realiza a requisição para listar templates
    function listaTemplateDocumento(){

        $.post(api+"controlador_geral.php?token="+token+"&operacao=listar&controlador=Template",JSON.stringify(data))
        .always(function( resposta ){

            if(resposta['success']){
                console.log(resposta);
            }
        });
    }

    //Desabilita edição
    function apenasLeitura(){
        $('#form-documento :input').attr('disabled', true);
    }

    //Realiza a requisição para exibir os detalhes de um documento
    function buscaDocumento(id_documento){
        $.post(api+"controlador_geral.php?token="+token+"&operacao=buscar&controlador=Documento&id="+id_documento,JSON.stringify(data))
        .always(function( resposta ){
            console.log("buscando "+id_documento);
            if(resposta['success']){
                console.log("a"+resposta['documento']);
                return resposta['documento'];
            }else{
                console.log("Erro")
            }
        });
    }


    function exibeDocumento(){

        $.post(api+"controlador_geral.php?token="+token+"&operacao=buscar&controlador=Documento&id="+id,JSON.stringify(data))
        .always(function( resposta ){

            if(resposta['success']){
                resposta = resposta["documento"];

                recuperaAssuntos();
                recuperaTipoDocumento();
                recuperaSetores();
                recuperaNiveisDeAcesso();

                for(campo in resposta){
                    $("input[name='"+campo+"']").val(resposta[campo]);
                    $("textarea[name='"+campo+"']").val(resposta[campo]);
                }

                $('select[name=procedencia] option[value=2]').prop('selected', true);
                escolheNivelAcesso(resposta["id_nivel_acesso"]);

                if(resposta["id_documento_template"] === null)
                    carregaTemplateDocumento(resposta["id_documento_tipo"]);
                else
                    carregaConteudoDocumento(resposta["id_documento_template"]);                    
            }

        });        
    }


    //Realiza a requisição para atualizar um documento
    function atualizaDocumento(){
        var json = $('#form-documento').serializeFormJSON();
        data.json = json;

        $.post(api+"controlador_geral.php?token="+token+"&controlador=Documento&operacao="+operacao+"&id="+id,JSON.stringify(data))
        .always(function( resposta ) {
            if(resposta["success"]){
                alert("Documento salvo!");
            }
        });
    }


    function listaDocumentosPorProcesso(){
        
        $.post(api+"documento.php?token="+token+"&operacao=documentos_por_processo&id="+id,JSON.stringify(data))
        .always(function( resposta ){

            if(resposta['success']){
                var documentos = resposta["documentos_por_processo"];

                for(var documento in documentos) {

                    $(".documentos-anexados .tabela-documentos tbody").append(documentoRow(documentos[documento]));
                    $(".documentos-anexados .tabela-documentos tbody #edit-"+documentos[documento].id).replaceWith("<button disabled onclick=\"removeDocumento(<?php echo $id_processo; ?>,"+documentos[documento].id+")\" class=\"btn btn-danger btn-sm d-block d-sm-inline\"><i class=\"fas fa-trash-alt\"></i> Remover</button>");
                   
                }
            }
        });
    }

    //Realiza a requisição para listar documentos
    function listaDocumento(){

        $.post(api+"controlador_geral.php?token="+token+"&operacao="+operacao+"&controlador=Documento",JSON.stringify(data))
        .always(function( resposta ){

            if(resposta['success']){
                var documentos = resposta["documentos"];

                for(var documento in documentos) {

                    $(".todos-documentos .tabela-documentos tbody").append(documentoRow(documentos[documento]));
                   
                }
            }
        });
    }

    function documentoRow(documento){
        return "<tr class=\"row\">"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Tipo:  </span>"+selecionaTipoDocumento(documento.id_documento_tipo)+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Autor:  </span>"+documento.autor+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Origem:  </span>"+documento.procedencia+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Criação:  </span>"+documento.data_recebimento+"</td>"+
                    "<td class=\"col-sm-2\"><span class=\"d-inline d-sm-none\">Assinado:  </span>"+verificaAssinatura(documento.assinado)+"</td>"+
                    "<td class=\"col-sm-2\"><a class=\"btn btn-info btn-sm d-block d-sm-inline\" target=\"_blank\" href=\"detalhes-documento.php?id="+documento.id+"\"><i class=\"fa fa-eye\"></i> Ver</a> <a id=\"edit-"+documento.id+"\" class=\"btn btn-info btn-sm\" target=\"_blank\" href=\"editar-documento.php?id="+documento.id+"\"><i class=\"fas fa-edit\"></i> Editar</a></td>"+
                "</tr>";
    }

</script>