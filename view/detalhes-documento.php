<?php
    $titulo = "Detalhes do documento";
    $descricao = "Detalhes do documento.";
    $assunto = "documento";
    $operacao ="alterar";
?>

<?php include 'parciais/var.php';?>
<?php include 'parciais/head.php';?>
<?php include 'parciais/topo-mobile.php';?>
<?php include 'parciais/menu-lateral.php';?>
<?php include 'parciais/topo.php';?>

<!-- Conteúdo -->
<div class="container-fluid">
    <div class="row topo-view">                        
            <div class="col-sm-7">
                <h2>Detalhes do documento</h2>
                <p class="info">
                    Veja e edite os detalhes do documento.
                <p> 
            </div>            
            <div class="col-sm-5 info">
                <button class="btn-enviar au-btn au-btn-icon btn-primary float-right" data-toggle="modal" data-target="#enviarDocumento">
                    <i class="fas fa-paper-plane"></i> Enviar Documento
                </button>
                <div class="dropdown show">
                  <a class="au-btn au-btn-icon btn-secondary float-right dropdown-toggle" href="#" role="button" id="menuDocumento" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-bars"></i> Opções
                  </a>

                  <div class="dropdown-menu" aria-labelledby="menuDocumento">
                    <a class="dropdown-item" href="editar-documento.php?id=<?php echo intval($_GET['id']);?>">
                        <i class="fas fa-edit"></i> Editar documento
                    </a>
                    <button class="dropdown-item" data-toggle="modal" data-target="#autenticarDocumento"><i class="fas fa-magic"></i> Autenticar documento</button>
                    <button class="dropdown-item" onclick="assina()">
                            <i class="fas fa-pen-nib"></i> Assinar documento
                        </button>
                  </div>
                </div>  
    </div>
    <div class="default-tab">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="processo-tab" data-toggle="tab" href="#processo" role="tab" aria-controls="processo" aria-selected="true">
                    <i class="far fa-file"></i> Documento
                </a>
                <a class="nav-item nav-link" id="documentos-tab" data-toggle="tab" href="#documentos" role="tab" aria-controls="documentos"  aria-selected="false">
                    <i class="fas fa-align-left"></i> Detalhes do documento
                </a>
                <a class="nav-item nav-link" id="historico-tab" data-toggle="tab" href="#historico" role="tab" aria-controls="historico"     aria-selected="false">
                    <i class="fas fa-history"></i> Histórico de tramitação
                </a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <!-- Visualização do documento -->
            <div class="tab-pane fade show active container" id="processo" role="tabpanel" aria-labelledby="processo-tab">
                <div class="row">                        
                    <?php include 'parciais/ver-documento.php';?>
                </div>
            </div>
            <!-- Fim da Visualização do documento  -->

            <!-- Meta dados do documento -->
            <div class="tab-pane fade container documentos-tab" id="documentos" role="tabpanel" aria-labelledby="documentos-tab">                            
                <?php include 'parciais/formulario-documento.php';?>
            </div>
            <!-- Fim dos Meta dados do documento -->
            
            <!-- Tabela de historico -->
            <div class="tab-pane fade container historico-tab" id="historico" role="tabpanel" aria-labelledby="historico-tab">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Histórico de Tramitação</h3>
                        </div>
                    </div>
                    <?php include "parciais/tabela-historico.php";?>
                    <div class="row">
                        <div class="col-sm-12">
                            <button class="btn-enviar au-btn au-btn-icon btn-primary" data-toggle="modal" data-target="#enviarDocumento">
                                <i class="fas fa-paper-plane"></i> Enviar documento
                            </button> 
                        </div>
                    </div>
                </div>
            </div>


            <!-- Fim do histórico -->
        </div>
    </div>    
</div>

<!-- Modal tramitação -->
<div class="modal fade" id="enviarDocumento" tabindex="-1" role="dialog" aria-labelledby="enviarDocumentoLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="enviarDocumentoLabel">Enviar documento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php include 'parciais/formulario-tramitacao.php';?>
            </div>                      
        </div>
    </div>
</div>

<!-- Modal autenticação -->
<div class="modal fade" id="autenticarDocumento" tabindex="-1" role="dialog" aria-labelledby="autenticarDocumentoLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="autenticarDocumentoLabel">Autenticar documento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php include 'parciais/formulario-autenticacao.php';?>
            </div>                      
        </div>
    </div>
</div>

<!-- Fim do Conteúdo -->

<?php include 'parciais/rodape.php';?>
<?php include 'parciais/scripts.php';?>
<?php include 'controlador/documento.php';?>

<script type="text/javascript">    
    $(exibeDocumento());
    $(apenasLeitura());
    $(recuperaHistorico());
</script>

	</body>
</html>