<?php
    $titulo = "Adicionar documentos ao processo";
    $descricao = "Adicione documentos a um processo.";
    $assunto = "processo";
    $id_processo = intval($_GET['id']);
    $operacao = "criar";
?>

<?php include 'parciais/var.php';?>
<?php include 'parciais/head.php';?>
<?php include 'parciais/topo-mobile.php';?>
<?php include 'parciais/menu-lateral.php';?>
<?php include 'parciais/topo.php';?>

<!-- Conteúdo -->
<div class="container-fluid">
    <div class="row">                        
        <div class="col-sm-12">
            <h3 id="titulo-processo">Adicionar documentos ao processo: </h3>
            <p class="info">
                Você pode adicionar novos documentos a um processo, assim como documentos já cadastrados.
            <p>
                        
        </div>
        <div class="container-fluid">
            <?php include 'parciais/tabela-documentos.php';?>            
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button id="novoDocumentoBtn" class="btn btn-primary btn-xl" data-toggle="modal" data-target="#novoDocumento"><i class="fa fa-plus-circle"></i> Novo documento</button>
            </div>
        </div>
    </div>
    
    <!-- Adicionar novo documento -->
    <div class="modal fade" id="novoDocumento" tabindex="-1" role="dialog" aria-labelledby="novoDocumentoLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="novoDocumentoLabel">Novo documento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php include 'parciais/formulario-documento.php';?>
                </div>                      
            </div>
        </div>
    </div>

<!-- Fim do Conteúdo -->

<?php include 'parciais/rodape.php';?>
<?php include 'parciais/scripts.php'?>
<?php include 'controlador/documento.php'?>
<?php include 'controlador/processo.php'?>



<script>

    function adicionaDocumento(id_processo,id_documento){
        var json = new Object();
        json.id_documento = id_documento;
        json.id_processo = id_processo;
        json.data_insercao = "2018/06/10";
        json.login_insercao = "fulano.tal";
        json.observacoes = "";

        data.json = json;

        $.post( "<?php echo $api; ?>processo_documento.php?token=lsdfjklsdfjsdlfjsdlfsdfsdf&operacao=criar", JSON.stringify(data))
          .always(function( resposta ) {
            if(resposta['success']){
                alert("Documento inserido com sucesso!");
                location.reload();
            }
          });
        

    }

    $( function(){

        $.post( "<?php echo $api; ?>controlador_geral.php?token=lsdfjklsdfjsdlfjsdlfsdfsdf&operacao=buscar&controlador=Processo&id=<?php echo $id_processo;?>", JSON.stringify(data))
        .always(function( resposta ){

            if(resposta['success']){
               resposta = resposta["processo"];

               $("#titulo-processo").append(resposta['nup']);   
            }
        });

        $.post( "<?php echo $api; ?>documento.php?token=lsdfjklsdfjsdlfjsdlfsdfsdf&operacao=documentos_sem_processo", JSON.stringify(data))
        .always(function( resposta ){

            if(resposta['success']){
                var documentos = resposta["documentos_sem_processo"];
                console.log(documentos);

                for(var documento in documentos) {

                    $(".tabela-documentos tbody").append(""+
                    "<tr class=\"row\">"+
                    "<td class=\"col-sm-2\"><a href=\"detalhes-documento.php?id="+documentos[documento].id+"\"> "+selecionaTipoDocumento(documentos[documento].id_documento_tipo)+"</a>"+"</td>"+
                    "<td class=\"col-sm-2\">"+documentos[documento].autor+"</td>"+
                    "<td class=\"col-sm-2\">"+documentos[documento].procedencia+"</td>"+
                    "<td class=\"col-sm-2\">"+documentos[documento].data_recebimento+"</td>"+
                    "<td class=\"col-sm-2\">"+verificaAssinatura(documentos[documento].assinado)+"</td>"+
                    "<td class=\"col-sm-2\"><button onclick=\"adicionaDocumento(<?php echo $id_processo; ?>,"+documentos[documento].id+")\" class=\"btn btn-success btn-sm\"><i class=\"fa fa-plus\"></i></button> <a class=\"btn btn-info btn-sm\" target=\"_blank\" href=\"detalhes-documento.php?id="+documentos[documento].id+"\"><i class=\"fa fa-eye\"></i></a></td>"+
                    "</tr>");
                   
                }
            }
        });
      });

</script>
