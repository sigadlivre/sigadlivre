<form id="form-tramitacao">
    
    <div class="row">
        <div class="col-sm-6 form-group">
            <label class="control-label" for="procedencia">Destinatário (Orgão):</label>
            <select class="form-control" name="procedencia" required="required">
            </select>
        </div>
        <div class="col-sm-6 form-group">
            <label class="control-label" for="usuario_interessado">Usuário(s) interessado(s):</label>
            <select class="form-control" name="usuario_interessado" required="required">
                <option value="fulano.tal">Fulano de Tal</option>
            </select>
        </div>
    </div>
    <div class="row">                        
        <div class="col-sm-12 form-group"> 
            <label class="control-label" for="observacoes">Observacoes:</label>
            <textarea class="form-control" name="observacoes" cols="40" rows="7"></textarea>
        </div>
    </div>

    <div class="row">                        
        <div class="col-sm-12 form-group">
            <button class="au-btn au-btn-icon btn-primary btn-sm" onclick="envia()"><i class="far fa-paper-plane"></i> Enviar</button>
            <button class="au-btn au-btn-icon btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
        </div>
    </div>
</form>