        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.php">
                            <h1>SIGAD</h1>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-home"></i>Início</a>
                        </li>
                        <li>
                            <a class="dropdown-toggle" href="#" role="button" id="menuProcessos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-archive"></i>Processos
                            </a>
                            <div class="dropdown-menu menu-mobile" aria-labelledby="menuProcessos">
                                <a class="dropdown-item" href="criar-processo.php">Novo processo</a>
                                <a class="dropdown-item" href="processos.php">Todos os processos</a>
                                <a class="dropdown-item" href="meus-processos.php">Meus processos</a>
                            </div>                                                      
                        </li>
                        <li>
                            <a class="dropdown-toggle" href="#" role="button" id="menuDocumentos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-file"></i>Documentos
                            </a>
                            <div class="dropdown-menu" aria-labelledby="menuDocumentos">
                                <a class="dropdown-item" href="criar-documento.php">Novo documento</a>
                                <a class="dropdown-item" href="documentos.php">Todos os documentos</a>
                            </div>                                                      
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
