<table class="table container-fluid table-borderless table-striped table-earning tabela-historico">
    <thead>
        <tr class="row">
            <th class="col-sm-2">Origem</th>
            <th class="col-sm-2">Destino</th>
            <th class="col-sm-2">Remetente</th>
            <th class="col-sm-3">Interessado(s)</th>
            <th class="col-sm-3">Data de envio</th>
        </tr>
    </thead>
    <tbody id="historico-processo">
    </tbody>
</table>