<form id="form-processo" method="post" action="">
    <div class="row">                        
        <div class="col-sm-6 form-group">                                
            <label class="control-label" for="titulo">Titulo:</label>
            <input class="form-control" type="text" name="titulo" required="required">
        </div>
        <div class="col-sm-6 form-group">
            <label class="control-label" for="nup">NUP:</label>
            <input  class="form-control" name="nup" disabled placeholder="0000 0000 0000 0000">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label class="control-label" for="interessado">Interessado:</label>
            <input class="form-control" list="interessados" name="interessado" required="required">
            <datalist id="interessados"></datalist>
        </div>
        <div class="col-sm-6 form-group"> 
            <label class="control-label" for="data_recebimento">Data de recebimento:</label>
            <input class="form-control" type="text" id="data_recebimento" name="data_recebimento" required="required">
        </div>
    </div>
    <div class="row">                        
        <div class="col-sm-12 form-group">
            <label class="control-label" for="procedencia">Procedência:</label>
            <select class="form-control" name="procedencia" disabled>
            </select>
        </div>
    </div>
    <div class="row"> 
        <div class="col-sm-12 form-group">
            <label class="control-label" for="assunto">Assunto:</label>
            <select class="form-control"  name="id_assunto" required="required"></select>
        </div>
    </div>
    <div class="row">                        
        <div class="col-sm-3 form-group">
            <label class="control-label" for="id_meio">Meio:</label>
            <select class="form-control" onchange="selecionaMeio(this)" name="id_meio" required="required">
                <option value="1">Digital</option>
                <option value="2">Hibrido</option>
                <option value="3">Fisico</option>
            </select>
        </div>
        <div class="col-sm-9 form-group" id="meioHibrido" style="display: none">  
            <label class="control-label" for="arquivo_processo">Envie o arquivo do processo:</label>
            <input id="meioHibridoCampo" class="form-control" type="file" accept="application/pdf" name="arquivo_processo">
        </div>
        <div class="col-sm-9 form-group" id="meioFisico" style="display: none">  
            <label class="control-label" for="localização">Localização do processo:</label>
            <input id="meioFisicoCampo" class="form-control" type="text" name="localização">
        </div>
    </div>
     <div class="row">                     
        <div class="col-sm-3 form-group">
            <label class="control-label" for=" nivel_acesso">Nivel de acesso:</label><br>
            <div class="form-check form-check">
                <div class="nivel-acesso"><input id="publico" class="form-check-input" onchange="exibeLegislacao(false)" type="radio" name="id_nivel_acesso" required="required" value="1">Público</div>
                <div class="nivel-acesso"><input id="sigiloso" class="form-check-input" onchange="exibeLegislacao(true)" type="radio" name="id_nivel_acesso" required="required" value="2">Sigiloso</div>
            </div> 
        </div>         
        <div class="col-sm-9 form-group" id="legislacao" style="display: none">  
            <label class="control-label" for="nivel_acesso_legislacao">Legislação do nível de acesso:</label>
            <select id="legislacaoCampo" class="form-control" name="id_nivel_acesso_legislacao">                    
            </select>
            <small>* Caso não encontre uma legislação adequada o processo deve ser público.</small>
        </div>
    </div>
    <div class="row">                        
        <div class="col-sm-12 form-group"> 
            <label class="control-label" for="observacoes">Observacoes:</label>
            <textarea class="form-control" name="observacoes" cols="40" rows="7"></textarea>
        </div>
    </div>
    
    <!-- Campos que serão preenchidos pelo sistema -->
    <dir>
        <input type="hidden" name="autor" value="<?php echo($login);?>">
        <input type="hidden" name="procedencia" value="<?php echo($id_setor);?>">
    </dir>

    <div class="row">                        
        <div class="col-sm-12 form-group">
            <button class="au-btn au-btn-icon btn-primary" type="submit"><i class="fas fa-save"></i> Salvar processo</button>
            <button class="au-btn au-btn-icon btn-danger" type="reset"><i class="fa fa-ban"></i> Limpar</button>
        </div>
    </div>
</form>