<!-- Jquery JS-->
<script src="tema/vendor/jquery-3.2.1.min.js"></script>

<!-- Bootstrap JS-->
<script src="tema/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="tema/vendor/bootstrap-4.1/bootstrap.min.js"></script>

<!-- Vendor JS       -->
<script src="tema/vendor/slick/slick.min.js">
</script>
<script src="tema/vendor/wow/wow.min.js"></script>
<script src="tema/vendor/animsition/animsition.min.js"></script>
<script src="tema/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="tema/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="tema/vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="tema/vendor/circle-progress/circle-progress.min.js"></script>
<script src="tema/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="tema/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="tema/vendor/select2/select2.min.js">
</script>


<script type="text/javascript">
	
	//Chama a função de seleção de datas
    $( function() {
        $( "#data_recebimento" ).datepicker({
            //Tradução dos termos do calendário
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
        });
      } );


    function recuperaAssuntos(){

        $.post(api+"controlador_geral.php?token="+token+"&operacao=listar&controlador=Assunto", JSON.stringify(data))
        .always(function( resposta ){
            if(resposta['success']){            	
                resposta = resposta["assuntos"];
                for(var i in resposta) {
                   $('[name="id_assunto"]').append("<option value=\""+resposta[i].id+"\">"+resposta[i].assunto+"</option>");
                }
            }
        });
    }

    function recuperaNiveisDeAcesso(){

        $.post(api+"controlador_geral.php?token="+token+"&operacao=listar&controlador=Nivel_Acesso_Legislacao", JSON.stringify(data))
        .always(function( resposta ){
            if(resposta['success']){     
            	console.log(resposta);       	
                resposta = resposta["niveis_acesso_legislacao"];
                for(var i in resposta) {
                   $('[name="id_nivel_acesso_legislacao"]').append("<option value=\""+resposta[i].id+"\">"+resposta[i].nome+"</option>");
                }
            }
        });
    }

    function recuperaTipoDocumento(){

        $.post(api+"controlador_geral.php?token="+token+"&operacao=listar&controlador=Documento_Tipo", JSON.stringify(data))
        .always(function( resposta ){
            if(resposta['success']){
            	console.log(resposta);
            	resposta = resposta["tipos"];
                for(var i in resposta) {
                   $('[name="id_documento_tipo"]').append("<option value=\""+resposta[i].id+"\">"+resposta[i].nome+"</option>");
                }
            }
        });
    }

    function recuperaMeioDocumento(){

        $.post(api+"controlador_geral.php?token="+token+"&operacao=listar&controlador=Documento_Meio", JSON.stringify(data))
        .always(function( resposta ){
            if(resposta['success']){
            	resposta = resposta["meios"];
                for(var i in resposta) {
                   $('[name="id_meio"]').append("<option value=\""+resposta[i].id+"\">"+resposta[i].meio+"</option>");
                }
            }
        });
    }

    function recuperaSetores(){

        $.post(api+"controlador_geral.php?token="+token+"&operacao=listar&controlador=Setor", JSON.stringify(data))
        .always(function( resposta ){
            if(resposta['success']){
            	resposta = resposta["setores"];
                for(var i in resposta) {
                   $('[name="procedencia"]').append("<option class=\"id_setor\" value=\""+resposta[i].id+"\">"+resposta[i].nome+"</option>");
                }
            }
        });
    }

    function recuperaData(){
        var d = new Date();

        var mes = d.getMonth()+1;
        var dia = d.getDate();

        var hoje = dia + '/' +
            ((''+mes).length<2 ? '0' : '') + mes + '/' +
            ((''+d.getFullYear()).length<2 ? '0' : '') + d.getFullYear();

        return hoje;
    }

    function escolheNivelAcesso(id_nivel_acesso){
        if(id_nivel_acesso== 1){
            $("#publico").attr('checked', 'checked');
        }else if(id_nivel_acesso== 2){
            $("#sigiloso").attr('checked', 'checked');
        }
    }

    

</script>

<!-- Datepicker -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Main JS-->
<script src="tema/js/main.js"></script>