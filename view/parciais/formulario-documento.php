<form id="form-documento" method="post" action="">
    <div class="row">                        
        <div class="col-sm-6 form-group">                                
            <label class="control-label" for="titulo">Titulo:</label>
            <input class="form-control" type="text" name="titulo" required="required">
        </div>
        <div class="col-sm-6 form-group">                                
            <label class="control-label" for="tipo">Tipo:</label>
            <select class="form-control" name="id_documento_tipo" required="required">
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label class="control-label" for="destinatario">Destinatário:</label>
            <input class="form-control" list="destinatarios" name="destinatario" required="required">
            <datalist id="destinatarios"></datalist>
        </div>
        <div class="col-sm-6 form-group"> 
            <label class="control-label" for="data_recebimento">Data de recebimento:</label>
            <input class="form-control" type="text" id="data_recebimento" name="data_recebimento" required="required">
        </div>
    </div>
    <div class="row">                        
        <div class="col-sm-12 form-group">
            <label class="control-label" for="procedencia">Procedência:</label>
            <select class="form-control" name="procedencia" required="required" disabled="">
                </select>
        </div>
    </div>
    <div class="row"> 
        <div class="col-sm-12 form-group">
            <label class="control-label" for="assunto">Assunto:</label>
            <select class="form-control"  name="id_assunto" required="required">
            </select>
        </div>
    </div>
    <div class="row">                        
        <div class="col-sm-3 form-group">
            <label class="control-label" for="meio">Meio:</label>
            <select class="form-control meio" onchange="selecionaMeio(this)" name="id_documento_meio" required="required">
                <option value="1">Digital</option>
                <option value="2">Hibrido</option>
                <option value="3">Fisico</option>
            </select>
        </div>
        <div class="col-sm-9 form-group" id="meioHibrido" style="display: none">  
            <label class="control-label" for="documento">Envie o arquivo do documento:</label>
            <input id="meioHibridoCampo" class="form-control" type="file" accept="application/pdf" name="documento">
        </div>
        <div class="col-sm-9 form-group" id="meioFisico" style="display: none">  
            <label class="control-label" for="localização">Localização do documento:</label>
            <input id="meioFisicoCampo" class="form-control" type="text" name="localização">
        </div>
    </div>
     <div class="row">                     
        <div class="col-sm-3 form-group">
            <label class="control-label" for=" nivel_acesso">Nivel de acesso:</label><br>
            <div class="form-check">
                <div class="nivel-acesso"><input class="form-check-input" onchange="exibeLegislacao(false)" type="radio" name="id_nivel_acesso" id="publico" required="required" value="1">Público</div>
                <div class="nivel-acesso"><input class="form-check-input" onchange="exibeLegislacao(true)" type="radio" name="id_nivel_acesso" id="sigiloso" required="required" value="2">Sigiloso</div>
            </div> 
        </div>         
        <div class="col-sm-9 form-group" id="legislacao" style="display: none">  
            <label class="control-label" for="nivel_acesso_legislacao">Legislação do nível de acesso:</label>
            <select id="legislacaoCampo" class="form-control" name="id_nivel_acesso_legislacao" value="0">
            </select>
            <small>* Caso não encontre uma legislação adequada o documento deve ser público.</small>
        </div>
    </div>
    <div class="row">                        
        <div class="col-sm-12 form-group"> 
            <label class="control-label" for="observacoes">Observacoes:</label>
            <textarea class="form-control" name="observacoes" cols="40" rows="7"></textarea>
        </div>
    </div>
    
    <!-- Campos que serão preenchidos pelo sistema -->
    <dir>
        <input type="hidden" name="autor" value="<?php echo($login)?>">
        <input type="hidden" name="id_documento_status" value="1">
        <input type="hidden" name="procedencia" value="<?php echo($id_setor);?>">
    <!-- Campos que serão preenchidos pelo sistema -->
    </dir>

    <div class="row">                        
        <div class="col-sm-12 form-group">
            <button class="au-btn au-btn-icon btn-primary" type="submit"><i class="fas fa-save"></i> Salvar documento</button>
            <button class="au-btn au-btn-icon btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
        </div>
    </div>
</form>