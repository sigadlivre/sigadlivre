        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo text-center">
                <a href="index.php">
                    <h1>SIGAD</h1>
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="<?php if($assunto=="inicio") echo "active"; ?>">
                            <a href="index.php">
                                <i class="fas fa-home"></i>Início</a>
                        </li>
                        <li class="<?php if($assunto=="processo") echo "active"; ?> has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-archive"></i>Processos</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="criar-processo.php">Novo processo</a></li>
                                <li><a href="processos.php">Todos os processos</a></li>
                                <li><a href="meus-processos.php">Meus processos</a></li>
                            </ul>                            
                        </li>
                        <li class="<?php if($assunto=="documento") echo "active"; ?> has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-file"></i>Documentos</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="criar-documento.php">Novo documento</a></li>
                                <li><a href="documentos.php">Todos os documentos</a></li>
                                <!--<li><a href="meus-documentos.php">Meus documentos</a></li> -->
                            </ul>                            
                        </li>                        
                    </ul>
                </nav>
            </div>
        </aside>
