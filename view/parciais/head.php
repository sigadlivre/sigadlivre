<html lang="pt-br">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content=<?php echo $descricao; ?>>

    <!-- Title Page-->
    <title><?php echo $titulo ?> - SIGAD</title>

    <!-- Fontfaces CSS-->
    <link href="tema/css/font-face.css" rel="stylesheet" media="all">
    <link href="tema/vendor/font-awesome-5.5/css/all.css" rel="stylesheet" media="all">
    
    <link href="tema/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    
    <!-- Bootstrap CSS-->
    <link href="tema/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="tema/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="tema/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="tema/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="tema/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="tema/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="tema/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="tema/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Datepicker CSS-->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">    

    <!-- Main CSS-->
    <link href="tema/css/theme.css" rel="stylesheet" media="all">
    <link href="css/style.css" rel="stylesheet" media="all">

</head>
