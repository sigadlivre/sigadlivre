<form id="form-autenticacao">
    <div class="row">
        <div class="col-sm-6 form-group">
            <label class="control-label" for="tipo_autenticacao">Tipo de autenticação:</label>
            <select class="form-control" name="tipo_autenticacao" required="required">
                <option>Comparado com o original</option>
                <option>Comparado com o cópia simples</option>
            </select>
        </div>
        <div class="col-sm-6 form-group">
            <label class="control-label" for="autenticador">Autenticado por:</label>
            <input class="form-control" name="autenticador" required="required" value="<?php echo($nome); ?>" readonly>
        </div>
    </div>
    <div class="row">                        
        <div class="col-sm-12 form-group"> 
            <label class="control-label" for="observacoes">Observacoes:</label>
            <textarea class="form-control" name="observacoes" cols="40" rows="7"></textarea>
        </div>
    </div>

    <div class="row">                        
        <div class="col-sm-12 form-group">
            <button class="au-btn au-btn-icon btn-primary btn-sm" onclick="autentica()"><i class="fas fa-magic"></i> Autenticar</button>
            <button class="au-btn au-btn-icon btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
        </div>
    </div>
</form>