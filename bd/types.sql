CREATE TYPE document_type as 
(
    id integer, 
    titulo character varying (50), 
    data_recebimento date, 
    observacoes character varying(50), 
    destinatario character varying(30), 
    autor character varying(30), 
    procedencia integer, 
    id_assunto integer, 
    id_documento_meio integer, 
    id_documento_status integer, 
    id_documento_tipo integer,
    id_nivel_acesso integer, 
    id_nivel_acesso_legislacao integer,
    id_documento_template integer,
    id_documento_arquivo integer,
    id_documento_fisico integer,
	usuario_assinou character varying(30),
    setor_usuario_assinou integer,
    data_assinatura timestamp without time zone,
	usuario_autenticou character varying(30),
    setor_usuario_autenticou integer,
	data_autenticacao timestamp without time zone,
    tipo_autenticacao character varying (50)
);

