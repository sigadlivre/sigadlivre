Documento:
{
   "json":{
      "titulo":"teste documento",
      "data_recebimento":"2018/06/10",
      "id_assunto":"100",
      "observacoes":"nenhuma",
      "destinatario":"fulano.tal",
      "autor":"fulano.tal",
      "procedencia":"fulano.tal",
      "id_documento_meio":"1",
      "id_documento_status":"3",
      "id_documento_tipo":"1",
      "id_nivel_acesso":"1"
   }
}
------------------------------------------

Processo: 
{
   "json":{
      "titulo":"teste processo",
      "data_recebimento":"2018/06/10",
      "assunto":"teste assunto",
      "interessado":"fulano.tal",
      "id_meio":"1",
      "id_nivel_acesso":"1",
      "autor":"fulano.tal",
      "procedencia":"fulano.tal"
   }
}
-------------------------------------------

Processo Documento:
{
   "json":{
      "id_processo":"",
      "id_documento":"",
      "data_insercao":"2018/06/10",
      "observacoes":"",
      "login_insercao":"fulano.tal"
   }
}
---------------------------------------------

Usuário:
{
   "json":{
      "login":"",
      "senha":""
   }
}
---------------------------------------------

Tramitação : 
{
   "json":{
      "id_processo":"1",
      "id_setor_origem":"1",
      "id_setor_destino":"2",
      "usuario_tramitou":"fulano.tal",
      "usuario_interessado":"roberto.silva",
      "data":"2018/10/06",
      "recebido":"false"
   }
}
-----------------------------

Documento Assinatura:
{
   "json":{
      "usuario":"",
      "id_setor":"",
      "id_documento":"",
      "data":""
   }
}