CREATE TABLE assunto(
  id integer primary key, assunto text, 
  pai integer
);

CREATE TABLE estado(
  id SERIAL PRIMARY KEY, 
  nome character varying(50) NOT NULL, 
  sigla character varying(2) NOT NULL
);

CREATE TABLE cidade(
  id SERIAL PRIMARY KEY, 
  nome character varying(50) NOT NULL
);

CREATE TABLE usuario(
  login character varying(30) PRIMARY KEY, 
  senha character varying(30) NOT NULL
);

CREATE TABLE endereco(
  id SERIAL PRIMARY KEY, 
  nome character varying(30) NOT NULL, 
  logradouro character varying(50) NOT NULL, 
  numero numeric NOT NULL, 
  complemento character varying(50), 
  cep character varying (8) NOT NULL, 
  id_cidade integer NOT NULL, 
  id_estado integer NOT NULL, 
  FOREIGN KEY (id_cidade) REFERENCES cidade(id), 
  FOREIGN KEY (id_estado) REFERENCES estado(id)
);

CREATE TABLE pessoa(
  id SERIAL PRIMARY KEY, 
  login character varying(30) NOT NULL, 
  nome character varying(50) NOT NULL, 
  email character varying(50) NOT NULL, 
  externo boolean NOT NULL, 
  id_endereco integer NOT NULL, 
  FOREIGN KEY (id_endereco) REFERENCES endereco(id), 
  FOREIGN KEY (login) REFERENCES usuario(login)
);

CREATE TABLE setor(
  id SERIAL PRIMARY KEY, 
  nome character varying(50) UNIQUE NOT NULL, 
  id_setor_pai integer, 
  FOREIGN KEY (id_setor_pai) REFERENCES setor(id)
);

CREATE TABLE papel(
  id SERIAL PRIMARY KEY, 
  nome character varying(30) NOT NULL, 
  descricao character varying(50) NOT NULL
);

CREATE TABLE usuario_setor(
  id SERIAL PRIMARY KEY, 
  login character varying(30), 
  id_setor INTEGER, 
  id_papel INTEGER, 
  FOREIGN KEY (login) REFERENCES usuario(login), 
  FOREIGN KEY(id_setor) REFERENCES setor(id), 
  FOREIGN KEY (id_papel) REFERENCES papel(id)
);

CREATE TABLE documento_arquivo(
  id SERIAL PRIMARY KEY, 
  nome VARCHAR(50) NOT NULL, 
  destino VARCHAR(50) NOT NULL, 
  data_upload DATE NOT NULL, 
  extensao VARCHAR(10) NOT NULL, 
  tamanho DECIMAL NOT NULL
);

CREATE TABLE template(
  id SERIAL PRIMARY KEY, 
  nome character varying(50) UNIQUE NOT NULL, 
  conteudo TEXT NOT NULL
);

CREATE TABLE documento_template(
  id SERIAL PRIMARY KEY, 
  data_criacao DATE NOT NULL, 
  conteudo TEXT NOT NULL, 
  id_template INTEGER NOT NULL, 
  FOREIGN KEY (id_template) REFERENCES template(id)
);

CREATE TABLE documento_tipo(
  id SERIAL PRIMARY KEY, 
  nome VARCHAR(30) NOT NULL, 
  descricao VARCHAR(30) NOT NULL
);

CREATE TABLE documento_meio(
  id SERIAL PRIMARY KEY, 
  nome VARCHAR(30) NOT NULL, 
  descricao VARCHAR(30) NOT NULL
);

CREATE TABLE documento_fisico(
  id SERIAL PRIMARY KEY, 
  nome_local VARCHAR(50) NOT NULL, 
  observacoes VARCHAR(50) NOT NULL
);

CREATE TABLE documento_status(
  id SERIAL PRIMARY KEY, 
  nome VARCHAR(30) NOT NULL, 
  descricao VARCHAR(30) NOT NULL
);

CREATE TABLE nivel_acesso(
  id SERIAL PRIMARY KEY, 
  nome VARCHAR (15) NOT NULL, 
  descricao VARCHAR(50) NOT NULL
);

CREATE TABLE nivel_acesso_legislacao(
  id SERIAL PRIMARY KEY, 
  nome VARCHAR (255) NOT NULL, 
  descricao VARCHAR(50)
);

CREATE TABLE classificacao(
  id SERIAL PRIMARY KEY, 
  classe character varying(50) NOT NULL, 
  id_classe_pai INTEGER, 
  FOREIGN KEY (id_classe_pai) REFERENCES classificacao(id)
);

CREATE TABLE documento(
  id SERIAL PRIMARY KEY, 
  titulo character varying(50) NOT NULL, 
  data_recebimento DATE NOT NULL, 
  observacoes character varying(50), 
  destinatario character varying (30) NOT NULL, 
  autor character varying(30) NOT NULL, 
  procedencia character varying(50) NOT NULL, 
  id_assunto INTEGER NOT NULL, 
  id_documento_meio INTEGER NOT NULL, 
  id_documento_status INTEGER NOT NULL, 
  id_documento_tipo INTEGER NOT NULL, 
  id_nivel_acesso INTEGER NOT NULL, 
  id_nivel_acesso_legislacao INTEGER, 
  id_documento_template INTEGER, 
  id_documento_arquivo INTEGER, 
  id_documento_fisico INTEGER, 
  FOREIGN KEY (destinatario) REFERENCES usuario(login), 
  FOREIGN KEY (autor) REFERENCES usuario(login), 
  FOREIGN KEY (id_documento_status) REFERENCES documento_status(id), 
  FOREIGN KEY (id_documento_meio) REFERENCES documento_meio(id), 
  FOREIGN KEY (id_nivel_acesso) REFERENCES nivel_acesso(id), 
  FOREIGN KEY (id_documento_arquivo) REFERENCES documento_arquivo(id), 
  FOREIGN KEY (id_documento_fisico) REFERENCES documento_fisico(id), 
  FOREIGN KEY (id_documento_template) REFERENCES documento_template(id), 
  FOREIGN KEY (id_documento_tipo) REFERENCES documento_tipo(id), 
  FOREIGN KEY (id_assunto) REFERENCES assunto(id)
);

CREATE TABLE processo(
  id SERIAL PRIMARY KEY, 
  nup character varying (30), 
  titulo character varying(50) NOT NULL, 
  data_recebimento DATE NOT NULL, 
  id_assunto INTEGER NOT NULL, 
  interessado character varying (30), 
  id_meio INTEGER NOT NULL, 
  id_nivel_acesso INTEGER NOT NULL, 
  id_nivel_acesso_legislacao INTEGER, 
  autor character varying (30) NOT NULL, 
  procedencia integer NOT NULL, 
  FOREIGN KEY (interessado) REFERENCES usuario(login), 
  FOREIGN KEY (autor) REFERENCES usuario(login), 
  FOREIGN KEY (id_meio) REFERENCES documento_meio(id), 
  FOREIGN KEY (id_nivel_acesso) REFERENCES nivel_acesso(id), 
  FOREIGN KEY (id_nivel_acesso_legislacao) REFERENCES nivel_acesso_legislacao(id), 
  FOREIGN KEY (id_assunto) REFERENCES assunto(id), 
  FOREIGN KEY (procedencia) REFERENCES setor(id)
);

CREATE TABLE processo_documento(
  id SERIAL PRIMARY KEY, 
  id_processo INTEGER NOT NULL, 
  id_documento INTEGER NOT NULL, 
  removido BOOLEAN DEFAULT false, 
  data_insercao DATE NOT NULL, 
  observacoes character varying(50), 
  login_insercao character varying(30), 
  FOREIGN KEY(login_insercao) REFERENCES usuario(login), 
  FOREIGN KEY (id_processo) REFERENCES processo(id), 
  FOREIGN KEY (id_documento) REFERENCES documento(id)
);

CREATE TABLE documento_assinatura(
  id SERIAL PRIMARY KEY, 
  usuario character varying(30), 
  id_setor INTEGER NOT NULL, 
  id_documento INTEGER, 
  data TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(), 
  FOREIGN KEY (usuario) REFERENCES usuario(login), 
  FOREIGN KEY (id_setor) REFERENCES setor(id), 
  FOREIGN KEY (id_documento) REFERENCES documento(id)
);

CREATE TABLE documento_autenticacao(
  id SERIAL PRIMARY KEY, 
  usuario character varying(30), 
  id_setor INTEGER NOT NULL, 
  id_documento INTEGER, 
  data TIMESTAMP WITHOUT ZONE NOT NULL DEFAULT now(), 
  tipo character varying(50) NOT NULL, 
  FOREIGN KEY (usuario) REFERENCES usuario(login), 
  FOREIGN KEY (id_setor) REFERENCES setor(id), 
  FOREIGN KEY (id_documento) REFERENCES documento(id)
);

CREATE TABLE nup(
  id SERIAL PRIMARY KEY, 
  tipo character varying(10) NOT NULL, 
  id_processo INTEGER, 
  id_documento INTEGER, 
  FOREIGN KEY (id_processo) REFERENCES processo(id), 
  FOREIGN KEY (id_documento) REFERENCES documento(id)
);

CREATE TABLE proc_doc_ciencia(
  id SERIAL PRIMARY KEY, 
  id_processo INTEGER, 
  id_documento INTEGER, 
  data DATE NOT NULL, 
  usuario character varying(30) NOT NULL, 
  FOREIGN KEY (usuario) REFERENCES usuario(login), 
  FOREIGN KEY (id_processo) REFERENCES processo(id), 
  FOREIGN KEY (id_documento) REFERENCES documento(id)
);

CREATE TABLE receber_processo(
  id SERIAL PRIMARY KEY, 
  id_processo INTEGER, 
  data DATE NOT NULL, 
  usuario character varying(30) NOT NULL, 
  FOREIGN KEY (usuario) REFERENCES usuario(login), 
  FOREIGN KEY (id_processo) REFERENCES processo(id)
);

CREATE TABLE atribuicao_processo(
  id SERIAL PRIMARY KEY, 
  id_processo INTEGER NOT NULL, 
  id_setor INTEGER NOT NULL, 
  usuario_atribuido character varying(30) NOT NULL, 
  data DATE NOT NULL, 
  FOREIGN KEY (id_processo) REFERENCES processo(id), 
  FOREIGN KEY (id_setor) REFERENCES setor(id), 
  FOREIGN KEY (usuario_atribuido) REFERENCES usuario(login)
);

CREATE TABLE tramitacao_processo(
  id SERIAL PRIMARY KEY, 
  id_processo INTEGER, 
  id_setor_origem INTEGER, 
  id_setor_destino INTEGER, 
  usuario_tramitou character varying(30), 
  usuario_interessado character varying(30), 
  data TIMESTAMP without time zone DEFAULT NOW(), 
  recebido boolean DEFAULT false, 
  usuario_recebeu character varying(30), 
  FOREIGN KEY (usuario_tramitou) REFERENCES usuario(login), 
  FOREIGN KEY (usuario_interessado) REFERENCES usuario(login), 
  FOREIGN KEY (usuario_recebeu) REFERENCES usuario(login), 
  FOREIGN KEY (id_processo) REFERENCES processo(id), 
  FOREIGN KEY (id_setor_origem) REFERENCES setor(id), 
  FOREIGN KEY (id_setor_destino) REFERENCES setor(id)
);

CREATE TABLE tramitacao_documento(
  id SERIAL PRIMARY KEY, 
  id_documento INTEGER, 
  id_setor_origem INTEGER, 
  id_setor_destino INTEGER, 
  usuario_tramitou character varying(30), 
  usuario_interessado character varying(30), 
  data TIMESTAMP without time zone DEFAULT NOW(), 
  recebido boolean DEFAULT false, 
  usuario_recebeu character varying(30), 
  FOREIGN KEY (usuario_tramitou) REFERENCES usuario(login), 
  FOREIGN KEY (usuario_interessado) REFERENCES usuario(login), 
  FOREIGN KEY (usuario_recebeu) REFERENCES usuario(login), 
  FOREIGN KEY (id_documento) REFERENCES documento(id), 
  FOREIGN KEY (id_setor_origem) REFERENCES setor(id), 
  FOREIGN KEY (id_setor_destino) REFERENCES setor(id)
);

CREATE TABLE processos_relacionados(
  id_processo INTEGER NOT NULL, 
  id_processo_relacionado INTEGER NOT NULL, 
  FOREIGN KEY (id_processo) REFERENCES processo(id), 
  FOREIGN KEY (id_processo_a_relacionar) REFERENCES processo(id)
);

CREATE TABLE cabecalho_template(
  id SERIAL PRIMARY KEY, conteudo text
);

