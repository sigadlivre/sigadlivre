CREATE VIEW processo_listagem (
  id, titulo, interessado, autor, procedencia, 
  nup, interessado
) AS (
  SELECT 
    p.id, 
    p.titulo, 
    p.interessado, 
    p.autor, 
    s.nome, 
    p.nup, 
    p.interessado 
  FROM 
    processo p, 
    setor s 
  WHERE 
    p.procedencia = s.id 
  ORDER BY 
    p.data_recebimento DESC
);

CREATE VIEW documento_listagem (
  id, titulo, tipo, autor, procedencia, 
  data_recebimento, destinatario, 
  assinado
) AS (
  SELECT 
    d.id, 
    d.titulo, 
    dt.nome AS tipo, 
    d.autor, 
    s.nome AS procedencia, 
    d.data_recebimento, 
    d.destinatario, 
    (
      d.id = documento_assinatura.id_documento
    ) AS assinado 
  FROM 
    documento d 
    JOIN setor s ON d.procedencia = s.id 
    LEFT JOIN documento_assinatura ON d.id = documento_assinatura.id_documento 
    LEFT JOIN documento_tipo dt ON dt.id = d.id_documento_tipo 
  ORDER BY 
    d.data_recebimento DESC
);
