CREATE FUNCTION busca_documento(id_doc int) returns SETOF document_type as $$ 
BEGIN
   RETURN QUERY 
   select
      d.*,
      da.usuario as usuario_assinou,
      da.id_setor as usuario_setor_assinou,
      da.data as data_assinatura,
      dau.usuario as usuario_autenticou,
      dau.id_setor as usuario_setor_autenticou,
      dau.data as data_autenticacao,
      dau.tipo as tipo_autenticacao 
   FROM
      documento d 
      LEFT JOIN
         documento_assinatura da 
         ON (d.id = da.id_documento) 
      LEFT JOIN
         documento_autenticacao dau 
         ON (d.id = dau.id_documento) 
   WHERE
      d.id = id_doc;
END;
$$ language 'plpgsql';
